<%@ Page Language="C#" MasterPageFile="birdweb.master"%>

<%@ Register TagPrefix="sas" TagName="weekly" Src="components/blurb.ascx" %>
<%@ Register TagPrefix="sas" TagName="promo" Src="components/sas_promo.ascx" %>
<%@ Register TagPrefix="sas" TagName="search" Src="components/search_box.ascx" %>
<%@ OutputCache Duration="3600" VaryByParam="none" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e) {
        Page.Header.Controls.Add( Util.NewMetaTag( "og:title", "BirdWeb" ) );
        Page.Header.Controls.Add( Util.NewMetaTag( "og:url", "https://birdweb.org/birdweb/" ) );
        Page.Header.Controls.Add( Util.NewMetaTag( "og:image", "https://birdweb.org/birdweb/web_images/birdweb_logo_fb.jpg" ) );
        Page.Header.Controls.Add( Util.NewMetaTag( "og:description", "Birds Connect Seattle's Guide to the Birds of Washington State" ) );
        Page.Header.Controls.Add( Util.NewMetaTag( "og:site_name", "BirdWeb" ) );
        Page.Header.Controls.Add( Util.NewMetaTag( "og:type", "website" ) );
    }
</script>
<asp:Content ID='homepagenav' ContentPlaceHolderID='mainnav' runat='server'>
     <ul>
        <li id='menuhome'><a href="<%= Util.MakeUrl("")%>">Home</a></li>
        <li id='menubirds'><a href='<%= Util.MakeUrl("birds")%>'>Birds</a></li>
        <li id='menuecoregions'><a href='<%= Util.MakeUrl("sites")%>'>Birding Sites and Ecoregions</a></li>
    </ul>
</asp:Content>
<asp:Content ID='homesidenav' ContentPlaceHolderID="sidenav" runat='server'>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <article>
        <section id="homesearch">
            <p>
                <strong>BirdWeb:</strong> <em>Birds Connect Seattle's Guide to the Birds of Washington State</em>
            </p>
           <sas:search runat="server"></sas:search>
        </section>
        <section id="homemenu">
                <div class='unit size1of2'>
                    <h2>
                        Browse By:</h2>
                    <table><tr>
                       <td> <a href="birds">
                            <img src="web_images/link_browse_birds.jpg" alt="feathers" /></a></td>
                            <td> <span class="subhead"><a href="birds">
                                Birds</a></span> Accounts of Washington's bird species with images, maps,
                        and sounds.</td>
                    </tr>
                    <tr><td>
                        <a href="sites">
                            <img src="web_images/link_birding_sites.jpg" alt="map"/></a></td>
                            <td> <span class="subhead"><a href="sites">
                                Birding Sites and Ecoregions</a></span> Washington's ecoregions and
                        favorite birding sites in each.
                    </td></tr>
                    </table>
                </div>
                <div class='unit size1of2 lastUnit'>
                    <h2>
                        Learn More About:
                    </h2>
                    <table><tr>
                    <td>
                        <a href="specialconcern">
                            <img src="web_images/link_species_of_special_concern.jpg" alt="wing" /></a></td><td> <span class='subhead'>
                                <a href="specialconcern">Species of Special Concern</a></span> Washington
                        bird species listed by state and federal agencies and by Audubon.
                    </td>
                    </tr><tr>
                    <td>
                        <a href="resources">
                            <img src="web_images/link_birding_resources.jpg" alt="binoculars" /></a></td><td> <span class="subhead"><a
                                href="resources">Birding Resources</a></span> Birding organizations
                        and events in Washington and other useful references.
                    </td>
                    </tr>
                    </table>
                </div>
        </section>
            <section id="week">
                <div class='weekly unit size1of3'>
                    <sas:weekly ID="weeklyBirdCtrl" Type='Bird' runat="server"></sas:weekly>
                </div>
                <div class='weekly unit size1of3'>
                    <sas:weekly ID="weeklySiteCtrl" Type='Site' runat="server"></sas:weekly>
                </div>
                <div class='weekly unit size1of3 lastUnit'>
                    <sas:promo ID="promo" Location="homepage" runat="server"></sas:promo>
                </div>
            </section>
    </article>
</asp:Content>
