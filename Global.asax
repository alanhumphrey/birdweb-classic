<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="System.Configuration" %>

<script language="C#" runat="server">

		private string ParseUrlForType(string sPageUrl)
		{
			
			string sType=sPageUrl.Substring(sPageUrl.LastIndexOf("/")+1,sPageUrl.LastIndexOf(".aspx")-(sPageUrl.LastIndexOf("/")+1));			
			
			return sType;
			
		}	
		
		private void AddHit(string sData)
		{
			string ConnectionString = ConfigurationManager.AppSettings["connectionstring"];
			string SqlTextString="";
			string sDate=String.Format("{0:yyyy-MM-dd}", DateTime.Now);
			
			SqlTextString=String.Format("INSERT INTO search_log(entry_time,entry_type,entry_data) VALUES('{0}',2,'{1}')", sDate, sData);

			OdbcConnection con=new OdbcConnection(ConnectionString);
			OdbcCommand cmd = new OdbcCommand(SqlTextString,con);
			cmd.CommandType=CommandType.Text;
			try
			{
				con.Open();
				cmd.ExecuteNonQuery();
				con.Close();
			}
			catch
			{
				if(con.State==ConnectionState.Open)					
					con.Close();
			}
		}	
		
        
		void Application_EndRequest(Object sender, EventArgs e)
		{	
			string sId="";
			string sColValue="";	
			if(Request.QueryString["value"]=="search")
			{				
				string sData=ParseUrlForType(Request.Url.ToString());			
				sId=Request.QueryString["id"];													
				switch (sData.Trim())
				{
					case "bird_details" :
						sColValue=sId+";birds";				
						AddHit(sColValue);
						break;
					case "ecoregion_details" :
						sColValue=sId+";ecoregions";								
						AddHit(sColValue);
						break;
					case "birding_site_details" :
						sColValue=sId+";sites";												
						AddHit(sColValue);
						break;		
				}
			}
			
			
			
		}
        
        </script>				


