﻿$(document).ready(function () {
    $(".birdgallery a").hover(
        function () {
            var image = $(this).find("img");
            $(".photo img").first()
                           .attr("src", image.attr("data-large_file"))
                           .attr("title", image.attr("title"))
                           .attr("alt", image.attr("alt"));
            $(".photo a").first()
                      .attr("data-image_url", $(this).attr("data-image_url"))
                      .attr("data-width", $(this).attr("data-width"))
                      .attr("data-height", $(this).attr("data-height"));

            var credit = "&copy; ";
            if (image.attr("data-contributor_url").length > 0) {
                credit += "<a href='" + image.attr("data-contributor_url") + "'>" + image.attr("data-contributor") + "</a>";
            } else {
                credit += image.attr("data-contributor");
            }
            $(".photo .credit span").html(credit);
        },
        function () { }
    );
    $(".photo a").click(function () {
        var url = $(this).attr("data-image_url");
        var width = parseInt($(this).attr("data-width")) + 125;
        var height = parseInt($(this).attr("data-height")) + 150;
        window.open(url, "BirdWebLargerImage", "toolbar=no,scrollbars=yes,width=" + width + ",height=" + height);
        event.preventDefault();
    });

    $("#tabs").tabs();
    $("#tabs > div").each(function (index, val) {
        if ( $.trim($(val).html()).length == 0) {
            $("#tabs").tabs("disable", index);
        }
    });

    $(".familymembers a").click(function () {
        var current_tab = $("#tabs").tabs('option', 'selected');
        if (current_tab > 0) {
            $(this).attr("href", $(this).attr("href") + "?tab=" + current_tab);
        }
    });

    $("#filmstripholder").width($("#filmstrip li").size() * 39);
    if ($("#filmstripholder").width() < $("#filmstripscroll").width()) {
        $("#slider").hide();
    }

    $("#slider").slider({
        animate: true,
        slide: handle_slider_slide,
        change: handle_slider_change
    });

    function handle_slider_slide(event, ui) {
        var maxscroll = $("#filmstripscroll").attr("scrollWidth") - $("#filmstripscroll").width();
        $("#filmstripscroll").animate({
            scrollLeft: ui.value * (maxscroll / 100)
        }, 0);
    }

    function handle_slider_change(event, ui) {
        var maxScroll = $("#filmstripscroll").attr("scrollWidth") - $("#filmstripscroll").width();
        $("#filmstripscroll").animate({
            scrollLeft: ui.value * (maxScroll / 100)
        }, 'fast');
    }

    $(".map img, .map p").click(function () {
        $("#tabs").tabs("select", "#maps");
        $("html,body").animate({ scrollTop: $("#tabs").offset().top }, 500);
    });

    $("#infodialogorder, #infodialogfamily, #speciesofconcern").dialog({ autoOpen: false, resizable: false });
    $("#orderdialog").click(function () {
        $("#infodialogorder").dialog("option", "title", $("#orderdialog").text());
        $("#infodialogorder").dialog("option", "buttons", [{
            text: "Close",
            click: function () {
                $(this).dialog("close");
            }
        }]);

        $("#infodialogorder").dialog("open");
    });
    $("#familydialog").click(function () {
        $("#infodialogfamily").dialog("option", "title", $("#familydialog").text());
        $("#infodialogfamily").dialog("option", "buttons", [{
            text: "Close",
            click: function () {
                $(this).dialog("close");
            }
        }]);

        $("#infodialogfamily").dialog("open");
    });
    $("#speciesofconcernicon").click(function () {
        $("#speciesofconcern").dialog("option", "title", $("#commonname").text());
        $("#speciesofconcern").dialog("option", "width", 600);
        $("#speciesofconcern").dialog("option", "modal", true);
        $("#speciesofconcern").dialog("option", "buttons", [{
            text: "Close",
            click: function () {
                $(this).dialog("close");
            }
        }]);
        $("#speciesofconcern").dialog("open");
    });
});

