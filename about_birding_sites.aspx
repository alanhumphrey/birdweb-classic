<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - About Birding Sites" %>

<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        Birding Sites</h1>
    <p>
        The birding site summaries were written by local birders and often reflect their
        unique styles and interests. Your experience at a site may be different, but we
        hope you'll get to know both the birds and the habitats of the various ecoregions
        a bit better. There is no guarantee that you will see all &#8212; or even any! &#8212; of the
        birds mentioned in any particular birding site summary. Rarities are mentioned in
        birding site summaries merely for historical interest.
    </p>
    <p>
        <a href="https://birdsconnectsea.org/get-involved/go-birding/" target="_blank">Go Birding with Birding Connects Seattle!</a>
    </p>
</asp:Content>
