<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Search Results" %>
<%@ Register TagPrefix="sas" TagName="search" Src="components/Search.ascx" %>
<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    void Page_Load( object Source, EventArgs e ) {
        BirdWeb.LogSearchTerm( Request["terms"] );
        string category_match = BirdWeb.SearchForTerm(Request["terms"]);
        if (category_match != null) {
            string detail_page = "";
            switch (category_match) {
                case "s":
                    detail_page = "site/";
                    break;

                case "e":
                    detail_page = "ecoregion/";
                    break;
                    
                default:
                    detail_page = "bird/";
                    break;
            }
            detail_page += Util.EncodeName( Request["terms"].ToString() );
            Response.Redirect(Util.MakeUrl(detail_page), true);
        }
        DataBind();
    }

</script>

<asp:Content runat="server" ContentPlaceHolderID="sidenav" ID="sidenav">
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="content" ID="maincontent">
    <sas:db ID="BirdWeb" runat="server"></sas:db>
    <form runat="server">
        <h1>
            Search Results for: "<%# Request["terms"] %>"
        </h1>
        <sas:search ID="birds" runat="server" SearchTerms='<%# Request["terms"] %>' SearchFor="birds">
        </sas:search>
    </form>
</asp:Content>
