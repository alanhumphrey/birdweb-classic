<%@ Control Language="C#" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Odbc" %>

<script runat="server">

    private OdbcDataReader conn(string CommandText) {

        string ConnectionString = ConfigurationSettings.AppSettings["connectionstring"];
        OdbcConnection myConnection = null;

        try {
            myConnection = new OdbcConnection(ConnectionString);
            OdbcCommand myCommand = new OdbcCommand(CommandText, myConnection);

            myConnection.Open();

            return myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        } catch (Exception ex) {
            myConnection.Close();
        }

        return null;
    }

    public string getValue(object column_data) {

        string value = "No data at this time.";

        if (!column_data.Equals(DBNull.Value)) {
            value = column_data.ToString();
            string endash = Convert.ToChar(150).ToString();
            string emdash = Convert.ToChar(151).ToString();
            string leftSmartQuote = Convert.ToChar(147).ToString();
            string rightSmartQuote = Convert.ToChar(148).ToString();
            value = Regex.Replace(value, "\n", "<br/>");
            value = Regex.Replace(value, endash, "&#8211;");
            value = Regex.Replace(value, emdash, "&#8212;");
            value = Regex.Replace(value, leftSmartQuote, "&#8220;");
            value = Regex.Replace(value, rightSmartQuote, "&#8221;");
        }
        return value;
    }

    public OdbcDataReader BirdList() {

        string CommandText = "select i.image_file, bcn.bird_common_name, g.genus_scientific_name, sn.species_scientific_name, b.taxonomic_order, b.id " +
                             "from birds b, species_names sn, genera g, bird_common_names bcn, images i, image_size_variations isv " +
                             "where b.species_name_id = sn.id and b.genus_id = g.id and bcn.id = b.default_bird_common_name_id " +
                             "and b.image_id = isv.image_id and isv.smaller_image_id = i.id " +
                             "order by b.taxonomic_order";
        return conn(CommandText);
    }

    public OdbcDataReader BirdDetail(string bird_id, string county_id) {
        string CommandText = "select b.*, o.id order_id, f.id family_id, o.order_scientific_name, " +
                             "    f.family_scientific_name, g.genus_scientific_name, sn.species_scientific_name, " +
                             "    bcn.bird_common_name, i.image_file, im.image_file map_image, " +
                             "    sn.species_scientific_name, bcn.bird_common_name " +
                             "from birds b " +
                             "    join  genera g on b.genus_id = g.id " +
                             "    join families f on g.family_id = f.id " +
                             "    join orders o on f.order_id = o.id " +
                             "    join species_names sn on b.species_name_id = sn.id " +
                             "    join bird_common_names bcn on b.default_bird_common_name_id = bcn.id " +
                             "    join images i on b.image_id = i.id " +
                             "    join breeding_maps bm on b.id = bm.bird_id " +
                             "    join maps m on m.id = bm.map_id " +
                             "    join images im on im.id = m.image_id " +
                             "where b.id = " + bird_id +
                             " and bm.county_id = " + county_id ;
        return conn(CommandText);
    }

    public OdbcDataReader CountyMaps(string bird_id) {
        string CommandText = "select * from breeding_maps where bird_id = " + bird_id;
        return conn(CommandText);
    }  

</script>

