<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Bird Details" %>

<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="image" Src="components/image.ascx" %>
<%@ Register TagPrefix="sas" TagName="checklist" Src="components/BirdCheckList.ascx" %>
<%@ Register TagPrefix="sas" TagName="family" Src="components/SpeciesFamily.ascx" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private int tab_num = 0;
    private int TabNum {
        get { return tab_num; }
        set { tab_num = value; }
    }
    
    string birdID;
    public string BirdID {
        set { birdID = value; }
        get { return birdID; }
    }

    string birdName;
    public string BirdName {
        set { birdName = value; }
        get { return birdName; }
    }
    
    string bird_image;
    public string BirdImage {
        set { bird_image = value; }
        get { return bird_image; }
    }

    private string concern_row = "";
    private string ConcernRow {
        get {
            OdbcDataReader reader = BirdWebDBCtrl.SpeciesOfConcern(BirdID);
            if (reader.HasRows) {
                Hashtable concern_orgs = new Hashtable();
                concern_orgs.Add("national_audubon_watch", "");
                concern_orgs.Add("federal", "");
                concern_orgs.Add("state", "");
                concern_orgs.Add("washington_watch", "");
                while (reader.Read()) {
                    concern_orgs[reader["attribute_name"]] = reader["attribute_value"].ToString();
                }
                concern_row = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
                    concern_orgs["federal"],
                    concern_orgs["national_audubon_watch"],
                    concern_orgs["state"],
                    concern_orgs["washington_watch"]
                    );
            }
            reader.Close();
            return concern_row;
        }
    }

    void Page_Load(object Source, EventArgs e) {
        if (!IsPostBack) {
            if (Request.QueryString["tab"] != null) {
                TabNum = Convert.ToInt16(Request.QueryString["tab"]);
            }
            if (Request.QueryString["id"] != null) {
                BirdID = Request.QueryString["id"];
                string common_name = BirdWebDBCtrl.BirdName(BirdID);
                Response.RedirectPermanent("bird/" + common_name, true);
            }
            OdbcDataReader reader = BirdWebDBCtrl.BirdIdAndPicture(Request.QueryString["name"]);
            if (reader.HasRows) {
                reader.Read();
                BirdID = reader["bird_id"].ToString();
                BirdImage = reader["bird_image"].ToString();
            }
            reader.Close();
            
            BirdID = BirdWebDBCtrl.BirdIDFromName(Request.QueryString["name"]);
            Detail.DataSource = BirdWebDBCtrl.BirdDetail(BirdID);
            Detail.DataBind();
            FamilyMembersLeftNav.DataBind();
            FamilyMembersBody.DataBind();

            HtmlHead head = (HtmlHead)Page.Header;
            string description =  BirdName + " habitat, behavior, diet, migration patterns, conservation status, and nesting.";
            Util.WriteMetaTag( head, "description", description);
            head.Controls.Add(Util.NewMetaTag("og:description", description));
            head.Controls.Add(Util.NewMetaTag("og:site_name", "BirdWeb"));
            head.Controls.Add(Util.NewMetaTag("og:url", "http://birdweb.org/birdweb/bird/" + Util.EncodeName(BirdName)));
            head.Controls.Add(Util.NewMetaTag("og:type", "birdweb:bird"));
            head.Controls.Add(Util.NewMetaTag("og:image", "http://birdweb.org/birdweb/" + BirdImage));
            head.Controls.Add(Util.NewMetaTag("og:title", BirdName));

            head.Title = BirdName;
        }
    }

    private string MakeMap(string map) {
        string result = string.Format("<a href='#'><img height='92' width='109' src='{0}'></a>", Util.MakeUrl(map));
        result += "<p><a href='#'>Click to View</a></p>";
        return result;
    }

    private string Maps(string wa_map, string na_map) {
        string maps = "";
        if (wa_map.Equals("No data at this time.")) wa_map = "";
        if (na_map.Equals("No data at this time.")) na_map = "";

        if ( wa_map.Length > 0 || na_map.Length > 0) {
            maps = string.Format("<div class='map no_print'>");
            if (wa_map.Length > 0) {
                maps += MakeMap(wa_map);
            }
            if (na_map.Length > 0) {
                if (wa_map.Length > 0) {
                    maps += "<div></div>";
                }
                maps += MakeMap(na_map);
            }
            maps += "</div>";
        }
        return maps;
    }

    // show species of concern icon, if appropriate
    private string ShowBadge(string data) {
        if (data.Equals("No data at this time.") || data.Equals("")) return "";
        string result = "<li>";
        string icon = string.Format("<img src='{0}' alt='Species of Concern' title='Species of Concern' height=20 width=20/>", Util.MakeUrl("web_images/bw_species_icons_sosc.png"));
        result += string.Format("<a href='#' id='speciesofconcernicon'>{0}</a>",icon);
        result += "</li>";
        return result;
    }

    private object Badges() {
        string badges = "<ul id='badges'>";
        badges += ShowBadge(ConcernRow);
        badges += "</ul>";
        return badges;
    }

    string Entry(string title, string data) {
        if (data == "No data at this time.") data = "";
        string entry = "";
        if (data.Length > 0) {
            if (title.Equals("wa_map") || title.Equals("na_map")) {
            } else if (title.Equals("Sound")) {
                entry = "<label><button onclick=\"document.getElementById('player').play()\">Listen</button></label>";
                entry += string.Format("<audio id = 'player'><source src=\"{0}\"></audio>", Util.MakeUrl(data));
                entry += "<div class='credit no_print'><a href='" + Util.MakeUrl("audiosource") + "'>Source of Bird Audio</a></div>";
            } else {
                entry = string.Format("<label> {0}: </label>{1}", title, data);
            }
        }
        return entry;
    }


    string Section(string title, string data, string map) {

        if (data.Equals("No data at this time.")) data = "";
        if (map.Equals("No data at this time.")) map = "";

        string section = "";
        if (data.Length > 0) {

            if (map.Equals("wa_map") || map.Equals("na_map")) {
                string mapclass = "";
                string legend = "";
                if (map.Equals("na_map")) {
                    mapclass = "class='na_map'";
                    legend = string.Format("<img class='map_legend' src='{0}' alt='North America map legend'/>", Util.MakeUrl("web_images/species_map_legend.jpg"));
                }
                data = string.Format("{2}<img src='{0}'{1}/>", Util.MakeUrl(data), mapclass, legend);
            }
            section = string.Format("<div class='details'><h3>{0}</h3><p>{1}</p></div>", title, data);
        }
        return section;
    }

    string Section(string title, string data) {
        return Section(title, data, "");
    }
</script>
<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <sas:db ID="BirdWebDBCtrl" runat="server"></sas:db>
    <sas:family ID="FamilyMembersLeftNav" runat="server" BirdID="<%# BirdID %>" CurrentBirdCssClass="current_bird">
    </sas:family>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <a name="description"></a>
    <asp:Repeater ID="Detail" runat="server">
        <ItemTemplate>
            <sas:image ID="imageCtrl" runat="server" ImageID='<%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "image_id" ) ) %>'
                LinkToLargerImage="true" ImageType="B" BirdID='<%# DataBinder.Eval(Container.DataItem,"id") %>' />
            
            <div id="bird_summary">
            <h1 id="commonname">
                <%# BirdName = BirdWebDBCtrl.getValue( DataBinder.Eval( Container.DataItem, "common_name" ) ) %>
            </h1>
            <div id="scientific_name">
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "genus_scientific_name" ) ) %>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "species_scientific_name" ) ) %>
            </div>
            <div>
            <label>
                Order:
            </label>
            <a href='#' id='orderdialog'>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_scientific_name" ) ) %>
            </a>
            <div id='infodialogorder'>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_description" ) ) %>
                </div>
            </div>
            <div>
            <label>
                Family:
            </label>
            <a href='#' id='familydialog'>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "family_scientific_name" ) ) %>
            </a>
                       <div id='infodialogfamily'>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "family_description" ) ) %>
                </div>
            </div>
            <div>
            <%# Entry ( "Status", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "notes" ) ) ) %>
            </div>
            <%# Badges() %>
            <div>
            <%# Entry ( "Sound", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "sound_file" ) ) ) %>
            </div>
            </div>
            <%# Maps( BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "wa_map" ) ),BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "na_map" ) ) ) %>

            <div id="tabs">
                <ul>
                    <li><a href="#description">Description</a></li>
                    <li><a href="#life_history">Life History</a></li>
                    <li><a href="#status">Status</a></li>
                    <li><a href="#find_in_wa">Find in WA</a></li>
                    <li><a href="#maps">Maps</a></li>
                </ul>
                <div id="description">
                    <%# Section ( "General Description", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "identification" ) ) ) %>
                </div>
                <div id="life_history">
                    <%# Section ( "Song", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "voice" ) )) %>
                    <%# Section ( "Habitat", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "habitat" ) )) %>
                    <%# Section ( "Behavior", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "behavior" ) )) %>
                    <%# Section ( "Diet", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "diet" ) )) %>
                    <%# Section ( "Nesting", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "nesting" ) )) %>
                </div>
                <div id="status">
                    <%# Section ( "Migration Status", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "migration" ) )) %>
                    <%# Section ( "Conservation Status", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "conservation_status" ) )) %>
                </div>
                <div id="find_in_wa">
                    <%# Section("When and Where to Find in Washington", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "where_found")), "") %>
                    <sas:checklist ID="BirdCheckListCtrl" runat="server" BirdID='<%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) %>' />
                </div>
                <div id="maps">
                    <%# Section("Washington Range Map", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "wa_map")), "wa_map")%>
                    <%# Section("North American Range Map", BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "na_map")), "na_map")%>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <sas:family ID="FamilyMembersBody" runat="server" BirdID="<%# BirdID %>" ShowImage="true"
        ShowScientificName="true" ListCssClass="birdphotos"></sas:family>
        <div id='speciesofconcern'>
        <div id='concernpopup' class='ui-widget ui-corner-all'>
        <table>
        <tr>
        <th>Federal Endangered Species List</th><th>Audubon/American Bird Conservancy Watch List</th><th>State Endangered Species List</th><th>Audubon Washington Vulnerable Birds List</th>
        </tr>
        <%= ConcernRow %>
        </table>
        </div>
        <p><a href='<%= Util.MakeUrl("specialconcern") %>' hidefocus='hidefocus'>View full list of Washington State's Species of Special Concern</a></p>
        </div>
    <script type="text/javascript" src="<%= Util.MakeUrl("js/bird_detail.js")%>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var tab_number = <%= TabNum %>;
            $("#tabs").tabs("option", "selected", tab_number);
        });
    </script>
</asp:Content>
