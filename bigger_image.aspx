<%@ Page Language="C#" %>
<%@ Register TagPrefix="sas" TagName="image" Src="components/image.ascx" %>
<script runat="server">

    // display the larger version of an image
         void Page_Load(object Source, EventArgs e) {
             imageCtrl.DataBind();
         }

</script>
<!DOCTYPE html>
<html>
<head>
    <title>BirdWeb - Larger Image</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link href="css/style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <table>
        <tbody>
            <tr>
                <td>
                    <sas:image id="imageCtrl" ImageType='<%# Request["type"] %>' ImageID='<%# Request["id"] %>' runat="server"></sas:image>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="javascript: self.close ()">[Close]</a>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
