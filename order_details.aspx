<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Order Details" %>

<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="families" Src="components/FamilyGroup.ascx" %>

<script runat="server">

    void Page_Load( object Source, EventArgs e ) {
        string order_id;
        if (Request.QueryString["id"] != null) {
            order_id = Request.QueryString["id"];
            string name = BirdWebDBCtrl.OrderName(order_id);
            Response.RedirectPermanent("order/" + name, true);
        }
        order_id = BirdWebDBCtrl.OrderIDFromName( Request.QueryString["sci_name"] );
        Detail.DataSource = BirdWebDBCtrl.OrderDetail( order_id );
        Detail.DataBind();
    }

</script>

<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <sas:db ID="BirdWebDBCtrl" runat="server"></sas:db>
    <asp:Repeater ID="Detail" runat="server">
        <ItemTemplate>
            <h1>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_navigation_name" ) ) %>
            </h1>
            <label>
                Order:</label>
            <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_scientific_name" ) ) %>
            <br />
            <br />
            <label>
                <a name="description"></a>Description</label>
            <p>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_description" ) ) %>
            </p>
            <h2>
                <a name="families"></a>Families Found in Washington
            </h2>
            <sas:families ID="FamilyList" runat="server" OrderID='<%# DataBinder.Eval( Container.DataItem, "id" ) %>'>
            </sas:families>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
