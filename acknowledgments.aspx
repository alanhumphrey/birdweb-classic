<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - Acknowledgments" %>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <ul>
        <li><a href="audiosource">Audio Source</a></li>
    </ul>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        Credits    </h1>
    <p>
        BirdWeb, Birds Connect Seattle's online guide to the birds of Washington, would not have
        been possible without the tireless efforts of a team of talented volunteers. Birds Connect Seattle
        extends its heartfelt thanks to all those who contributed their professional
        expertise and hard work to make this public resource a reality.    </p>
    <p>
        The Science Committee of Birds Connect Seattle has guided BirdWeb since its conception
        in 1999. Phase I (begun July 2000, launched November 2002) saw the development of
        the "Birds" and "Species of Special Concern" sections. In Phase II (begun July 2004,
        launched November 2005), we have added the "Ecoregions" and "Birding Sites" sections,
        as well as numerous recordings of bird vocalizations.    </p>
    <p>
        The species distribution maps were created for BirdWeb by Kelly Cassidy. By special
        arrangement they were also used in <span class='title'>Birds of Washington,</span>
        edited by Terence R. Wahl, Bill Tweit, and Steven G. Mlodinow, published by Oregon
        State University Press (2005). In exchange, BirdWeb benefited from expert review
        of the maps by the team of field ornithologists who wrote the species accounts for
        the book.    </p>
    <p>
        Special thanks go to Lorraine Hartmann for heading the search to find our talented
        team of web volunteers; Chris Peterson and the BirdNote radio program for generously
        providing the funds to license the sound recordings for BirdWeb; Tom Munson for the bird images
        used in our graphic links; and the Board of Birds Connect Seattle for supporting this
        project.    </p>
    <p>Glenn Nelson provided the Short-eared Owl photo that graces our banner. <a target="_blank" href="https://trailposse.com/">trailposse.com</a></p>
<table id="credits">
        <tbody>
            <tr valign="center">
                <td valign="top" width="50%">
                    <h3>
                        Production Management Group<br />
                        (Phase I)                    </h3>
                    Hal Opperman, Chair<br />
                    Tom Aversa<br />
                    Ellen Blackstone<br />
                    Kelly Cassidy<br />
                    Brenda Senturia<br />
                    Ron Simons<br />
                    Emily Sprong<br />                </td>
                <td valign="top" width="50%">
                    <h3>
                        Production Management Group<br />
                        (Phase II)                    </h3>
                    Helle Bielefeldt-Ohmann, Chair<br />
                    Tom Aversa<br />
                    Ellen Blackstone<br />
                    Hal Opperman<br />
                    Dennis Paulson<br />
                    Adam Sedgley<br />
                    Brenda Senturia<br />
                    Ron Simons<br />
                    Margot Stiles<br />                </td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Staff Project Lead (Phase I)                    </h3>
                    Emily Sprong</td>
                <td valign="top">
                    <h3>
                        Staff Project Lead (Phase II)                    </h3>
                    Adam Sedgley<br />
                    Margot Stiles<br />                </td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Web Developers (Phase I)                    </h3>
                    Chihiro Nishihara - Designer<br />
                    Adam Ahringer<br />
                    Kelly Crimmins<br />
                    Steve Garvin<br />
                    Deni Mensing<br />
                    Andrea Sparling<br />
                    Shang-fan Tu                </td>
                <td valign="top">
                    <h3>
                        Web Developers (Phase II)                    </h3>
                    Alan Humphrey - Tech Lead<br />
                    Josh Freedman - Tech Lead
                    <br />
                    Praveen Chettypally<br />
                    Liang Cui<br />
                    Kristin Marshall<br />
                    Madhuritha Reddy<br />
                    Rochelle Ribeiro<br />
                    Dean Simpson<br />
                    Vanarasi Swamy<br />
                    Sri Veena Syam<br />                </td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Graphic Design                    </h3>
                    Adam Sedgley                </td>
                <td valign="top">                </td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Authors                    </h3>
                    <h4>
                        Birds                    </h4>
                    Emily Sprong - Order, Family and Species Accounts<br />
                    Hal Opperman - Rarities Accounts<br />
                    Jennifer Brookshier<br />
                    Marti Louther<br />
                    Brenda Senturia<br />
                    <h4>
                        Species of Special Concern                    </h4>
                    Emily Sprong<br />
                    Hal Opperman<br />
                    <h4>
                        Ecoregions                    </h4>
                    Helle Bielefeldt-Ohmann<br />
                    Hal Opperman<br />
                    Dennis Paulson<br />
                    <h4>
                        Seasonal Abundance Tables                    </h4>
                    <h5>
                        Compilers/Editors                    </h5>
                    Tom Aversa<br />
                    Hal Opperman<br />
                    Dennis Paulson<br />
                    <h5>
                        Regional Contributors                    </h5>
                    Jim Acton<br />
                    Gary Bletsch<br />
                    Wilson Cady<br />
                    Mike Denny<br />
                    Mark Houston<br />
                    Bob Kuntz<br />
                    Bill LaFramboise<br />
                    Nancy LaFramboise<br />
                    Doug Schonewald<br />
                    Dan Stephens<br />
                    Patrick Sullivan<br />
                    Charles Swift<br />
                    Bill Tweit<br />
                    Charlie Wright<br />                </td>
                <td valign="top">
                    <h4>
                        Birding Sites                    </h4>
                    <h5>
                        Compilers/Editors                    </h5>
                    Ellen Blackstone<br />
                    Brenda Senturia<br />
                    <h5>
                        Site Descriptions                    </h5>
                    Scott Atkinson<br />
                    Brian Bell<br />
                    Julia Bent<br />
                    Bob Boekelheide<br />
                    Ken Brunner<br />
                    Wilson Cady<br />
                    Harold Cole<br />
                    Ed Deal<br />
                    Mike Denny<br />
                    Michael Donahue<br />
                    Kevin Gleuckert<br />
                    Denny Granstrand<br />
                    Dave Hayden<br />
                    Randy Hill<br />
                    Michael Hobbs<br />
                    Scott Horton<br />
                    Barb Jensen<br />
                    Phil Kelley<br />
                    Kraig Kemper<br />
                    Gary Kuiper<br />
                    Robert Kuntz<br />
                    Garrett MacDonald<br />
                    Kelly McAllister<br />
                    Bob Morse<br />
                    Martin Muller<br />
                    Tim O'Brien<br />
                    Hal Opperman<br />
                    Pam Pritzl<br />
                    Penny Rose<br />
                    Doug Schonewald<br />
                    Brenda Senturia<br />
                    Gina Sheridan<br />
                    Ginger Shoemake<br />
                    Connie Sidles<br />
                    Dan Stephens<br />
                    Bob Sundstrom<br />
                    Kerry Turley<br />
                    Mike Wile<br />
                    Dan Willsie<br />
                    Charlie Wright</td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Technical Editors                    </h3>
                    Tom Aversa<br />
                    Michael Donahue<br />
                    Hal Opperman<br />
                    Dennis Paulson<br />
                    Brenda Senturia<br />                </td>
                <td valign="top">
                    <h3>
                        Audio Specialists                    </h3>
                    Jerry Joyce - Lead<br />
                    Adam Sedgley<br />
                    Isadora Ar�valo Wong<br />                </td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Copy Editors                    </h3>
                    Ellen Blackstone - Lead<br />
                    Hanna Atkins<br />
                    Dave Hayden<br />
                    Mary Rogers<br />
                    Brenda Senturia<br />
                    Connie Sidles<br />
                    Ron Simons<br />
                    Alice Skipton<br />
                    Sharon Sneddon<br />
                    Frances Wood<br />                </td>
                <td valign="top">
                    <h3>
                        Digital Image Specialists                    </h3>
                    Jennifer Leach - Lead<br />
                    Ilene Samowitz - Lead<br />
                    Robyn Allison<br />
                    Diane Dodd<br />
                    Boyd Fackler<br />
                    Kurt Miethke<br />
                    Catherine Reilly<br />
                    Linda Wakeman<br />
                    Nancy Wilson<br />                </td>
            </tr>
            <tr valign="center">
                <td valign="top">
                    <h3>
                        Mapmakers                    </h3>
                    Kelly Cassidy - Washington Range Maps<br />
                    Kenn Kaufman - North American Range Maps<br />
                    Cindy Lippincott - Ecoregion Maps<br />                </td>
                <td valign="top">
                    <h3>
                        Photo Sources                    </h3>
                    <h5>
                        Principal Contributors                    </h5>
                    Paul Bannick<br />
                    Michael Donahue<br />
                    Denny Granstrand<br />
                    Mike Hamilton<br />
                    Joseph V. Higbee<br />
                    William Mancebo<br />
                    Tom Munson<br />
                    Dennis Paulson<br />
                    Ilene Samowitz<br />
                    Netta Smith<br />
                    Ruth Sullivan<br />
                    <p>
                        The images on BirdWeb have been generously donated by a number of photographers,
                        most notably those listed above, and are individually credited on the site. To augment
                        the selection of images, Birds Connect Seattle has drawn from its extensive collection
                        of donated slides. We have attempted to identify photographers wherever possible,
                        and to obtain permission for use of their work. However, we were not able to determine
                        the rightful owner in every case. If you know the photographer of an image that
                        has been credited incorrectly, please contact <a href="mailto:birdweb@birdsconnectsea.org">
                            birdweb@birdsconnectsea.org</a>.                    </p>                </td>
            </tr>
            <tr valign="center">
                <td valign="top" height="90">
                    <h3>
                        Bird Silhouette Artist                    </h3>
                    Sue Trevathan                </td>
                <td valign="top">
                    <h3>
                        Bird Vocalization Recordings                    </h3>
                    <p>
                        <a href="http://macaulaylibrary.org/">The Macaulay Library of Natural
                            Sounds</a>, Cornell Laboratory of Ornithology, Ithaca, New York.                    </p>
                    <p>
                        Peter Ward and Ken Hall, from their CD <i>Songbirds of the Northwest: Nature Sounds
                        of British Columbia Washington and Oregon</i>.

                    </p>

                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
