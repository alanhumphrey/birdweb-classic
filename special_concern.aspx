<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Species of Special Concern" %>

<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<%@ OutputCache Duration="3600" VaryByParam="none" %>

<script runat="server">

    string speciesTable;
    public string SpeciesTable {
        get { return speciesTable; }
        set { speciesTable = value; }
    }

    string SubSpeciesName( string id, string name ) {

        switch ( id ) {
            case "138": // snowy plover
                name += "<br/>(Western subspecies)";
                break;

            case "248": // spotted owl
                name += "<br/>(Northern subspecies)";
                break;

            case "334": // white-breasted nuthatch
                name += "<br/>(Slender-billed subspecies)";
                break;

            case "409": // vesper sparrow
                name += "<br/>(Oregon subspecies)";
                break;

            case "485": // cackling goose
                name += "<br/>(Aleutian subspecies)";
                break;

            case "320": // horned lark
                name += "<br/>(Streaked subspecies)";
                break;

            case "239": // yellow billed cuckoo
                name += "<br/>(Western population)";
                break;

        }

        return name;
    }

    void Page_Load( object Source, EventArgs e ) {
        OdbcDataReader reader = BirdWebDBCtrl.SpeciesOfConcern();

        StringBuilder table = new StringBuilder();

        table.Append("<div id='concern'><table><tr><th width='30%'></th><th ><a href='http://www.fws.gov/endangered/' target='_blank'>Federal Endangered Species List</a></th>");
        table.Append("<th ><a href='https://abcbirds.org/threats/' target='_blank'>American Bird Conservancy's Threats to Birds</a></th>");
        table.Append("<th ><a href='http://wdfw.wa.gov/conservation/endangered/' target='_blank'>State Endangered Species List</a></th>");
        table.Append( "<th ><a href='https://www.audubon.org/climate/survivalbydegrees' target='_blank'>Audubon's Species on the Brink</a></th>" );
        //table.Append("<th>Washington Gap Analysis At-Risk List</th></tr>");

        Hashtable attributes = new Hashtable();
        attributes.Add( "federal", "--" );
        attributes.Add( "national_audubon_watch", "--" );
        attributes.Add( "state", "--" );
        attributes.Add( "washington_watch", "--" );
        //attributes.Add("gap_at_risk", "--");

        // variable for alternating rows
        Boolean altrow = false;
        string currentBirdName = "";
        string currentBirdID = "";
        while ( reader.Read() ) {
            if ( currentBirdID.Equals( reader[0].ToString() ) || currentBirdID.Equals( "" ) ) {
                attributes[reader["attribute_name"].ToString()] = reader["attribute_value"].ToString();

                currentBirdID = reader[0].ToString();
                currentBirdName = SubSpeciesName( reader["id"].ToString(), reader["common_name"].ToString() );
            } else {
                table.Append( AddRow( currentBirdName, currentBirdID, attributes, altrow ) );
                altrow = !altrow;
                currentBirdID = reader[0].ToString();
                currentBirdName = reader[1].ToString();
                attributes["federal"] = "--";
                attributes["national_audubon_watch"] = "--";
                attributes["state"] = "--";
                attributes["washington_watch"] = "--";
                //attributes[ "gap_at_risk" ] = "--";
                attributes[reader[2].ToString()] = reader[3].ToString();
            }
        }
        reader.Close();
        table.Append( AddRow( currentBirdName, currentBirdID, attributes, altrow ) );

        table.Append( "</table></div>" );
        SpeciesTable = table.ToString();
    }

    private string AddRow( string currentBirdName, string currentBirdID, Hashtable attributes, Boolean altRow ) {
        StringBuilder row = new StringBuilder();
        String endangeredStyle = "";

        if ( altRow ) {
            row.Append( "<tr class=altrow>" );
        } else {
            row.Append( "<tr>" );
        }

        // the regex strips out the "<br/>sub species name" added to some bird names in SubSpeciesName above.
        row.Append( String.Format( "<th class='birdname'><a href='bird/{0}'>{1}</a></th>", Util.EncodeName( Regex.Replace(currentBirdName,"(.*)(<.*)","$1").ToString() ), currentBirdName ) );

        // this code determines if the species is endangered and applies a special style (called "endangered") to it.
        if ( attributes["federal"].Equals( "Endangered" ) )
            endangeredStyle = "class=endangered";
        else
            endangeredStyle = "";
        row.Append( String.Format( "<td " + endangeredStyle + ">{0}</td>", attributes["federal"] ) );

        if ( attributes["national_audubon_watch"].Equals( "Red List" ) )
            endangeredStyle = "class=endangered";
        else
            endangeredStyle = "";
        row.Append( String.Format( "<td " + endangeredStyle + ">{0}</td>", attributes["national_audubon_watch"] ) );


        // this code determines if the species is endangered and applies a special style (called "endangered") to it.
        if ( attributes["state"].Equals( "Endangered" ) )
            endangeredStyle = "class=endangered";
        else
            endangeredStyle = "";
        row.Append( String.Format( "<td " + endangeredStyle + ">{0}</td>", attributes["state"] ) );

        // this code determines if the species is endangered and applies a special style (called "endangered") to it.
        if ( attributes["washington_watch"].Equals( "Immediate Concern" ) )
            endangeredStyle = "class=endangered";
        else
            endangeredStyle = "";
        row.Append( String.Format( "<td " + endangeredStyle + ">{0}</td>", attributes["washington_watch"] ) );

        //table.Append( String.Format( "<td class='altcol'>{0}</td>", attributes[ "gap_at_risk" ] ) );
        row.Append( "</tr>" );

        return row.ToString();
    }
</script>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <sas:db ID="BirdWebDBCtrl" runat="server"></sas:db>
        <h2 class="qlink">
            Listing Information
        </h2>
        <ul>
            <li><a href="http://www.fws.gov/endangered" target='_blank'>Federal Endangered Species Program (USFWS)</a>
            </li>
            <li><a href="https://abcbirds.org/threats/" target="_blank">American Bird Conservancy's Threats to Birds</a></li>
            <li><a href="http://wdfw.wa.gov/conservation/endangered/" target='_blank'>State Species of Concern
                (WDFW)</a> </li>
            <li><a href="https://www.audubon.org/climate/survivalbydegrees" target="_blank">Audubon's Species on the Brink</a> </li>
        </ul>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        Species of Special Concern
    </h1>
    <p>
        Many birds in Washington are showing signs of decline due to habitat loss and other
        environmental threats. Some of these species are still relatively common and widespread
        while others have all but disappeared from the state. After careful scientific monitoring,
        various government agencies and non-governmental organizations formally designate
        those species deemed most at risk, placing them on special lists in order to provide
        protection and call attention to their status. Designations from four of the most
        important lists are compiled in the table below: the federal List of Threatened
        and Endangered Species (U.S. Fish and Wildlife Service), the Audubon/American Bird Conservancy Watch List, the state Species of Concern
        list (Washington Department of Fish and Wildlife), and the Audubon Washington Vulnerable
        Birds list.
    </p>
    <p><strong>Click on the name at the head of the column for further details about each list, or on a species' name on the table to go to its BirdWeb account.</strong></p>
    <% Response.Write( SpeciesTable );%>
</asp:Content>
