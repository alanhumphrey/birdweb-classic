<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - File Not Found" %>

<script runat="server">
    void Page_Load(object Source, EventArgs e)
    {
        HtmlMeta description = new HtmlMeta();
        HtmlHead head = (HtmlHead)Page.Header;
        description.Name = "Description";
        description.Content = "Bird Web Error Page.";
        head.Controls.Add(description);
        head.Title = "Page Not Found - Birdweb";
    }
</script>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <div class="qlink">
        BirdWeb</div>
    <ul>
        <li><a href="birds">Browse Birds</a> </li>
        <li><a href="ecoregions">Browse by Ecoregion</a> </li>
        <li><a href="sites">Browse Birding Sites</a> </li>
        <li><a href="specialconcern">Species of Special Concern</a> </li>
        <li><a href="resources">Birding Resources</a> </li>
    </ul>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        404 Error - Page Not Found</h1>
    <p>
        Web pages are like birds<br />
        Always changing and on the move...
        <br />
        Well, you've found one that is not here.
    </p>
    <p>
        That's "404: File not found" for the technically inclined. For the not-so-technically
        inclined, that means that the link you clicked, or the URL you typed into your browser,
        didn't work for some reason. Here are some possible reasons why:
    </p>
    <ol>
        <li>We have a "bad" link floating out there and you were unlucky enough to click it.</li>
        <li>You may have typed the page address incorrectly.</li>
        <li>This web server is going nutty. </li>
    </ol>
    So now what?
    <p>
        You could either try the features on the left or do a search</p>
</asp:Content>
