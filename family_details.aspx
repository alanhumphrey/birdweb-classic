<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Family Details" %>

<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="birdlist" Src="components/SpeciesFamily.ascx" %>
<%@ Import Namespace="BirdWeb" %>

<script runat="server">

    void Page_Load( object Source, EventArgs e ) {
        string family_id;
        if (Request.QueryString["id"] != null) {
            family_id = Request.QueryString["id"];
            string name = BirdWebDBCtrl.FamilyName(family_id);
            Response.RedirectPermanent("family/" + name, true);
        }
        family_id = BirdWebDBCtrl.FamilyIDFromName( Request.QueryString["sci_name"] );
        Detail.DataSource = BirdWebDBCtrl.FamilyDetail( family_id );
        Detail.DataBind();
    }

    string FamilyName( string nav_name, string common_name ) {
        if ( nav_name.Equals( "No data at this time." ) ) {
            return common_name;
        } else {
            return nav_name;
        }
    }
</script>

<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <sas:db ID="BirdWebDBCtrl" runat="server"></sas:db>
    <asp:Repeater ID="Detail" runat="server">
        <ItemTemplate>
            <img style="float: right;" src='<%# Util.MakeUrl(DataBinder.Eval(Container.DataItem, "image_file" ).ToString() ) %>' />
            <h1>
                <%# FamilyName(BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "family_navigation_name" ) ),BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "family_common_name" ) ) ) %>
            </h1>
            <label>
                Family:</label>
            <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "family_scientific_name" ) ) %>
            <br />
            <label>
                Order:</label>
            <a href='<%# Util.MakeUrl("order/" + Util.EncodeName( BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_scientific_name" ).ToString() ) ) ) %>'>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "order_scientific_name" ) ) %>
            </a>
            <br />
            <br />
            <h2>
                Description
            </h2>
            <p>
                <%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "family_description" ) ) %>
            </p>
            <sas:birdlist runat="server" FamilyID='<%# BirdWebDBCtrl.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) %>' ListCssClass="birdphotos" ShowImage=true ShowScientificName=true Title="Species Found In Washington" />
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
