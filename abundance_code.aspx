<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - Abundance Codes" %>

<script runat="server">

    private string fromPage;
    public string FromPage {
        get { return fromPage; }
        set { fromPage = value; }
    }

    void Page_Load( object Source, EventArgs e ) {
        FromPage = Request.QueryString["page"];
    }
    private string Explanation() {
        string text;
        if ( FromPage.Equals( "bird_detail" ) ) {
            text = "<p>The table shows an <em>average</em> status for months during which the abundance of a migratory species increases or decreases. For example, a species that is Rare in the first week of April but becomes Fairly Common by the last week may be shown as Uncommon for that month.</p>";
            text += "<p>Click on an ecoregion&#8217;s name to learn about its climate and topography, flora and fauna, and good places to look for the birds that live there. While many bird species will be of similar abundance in a given month throughout an ecoregion, some will not, perhaps occurring only in the southern or northern part, or in limited habitats. In such cases, the table shows the status for the part of the region where the species is <em>most abundant</em> in that month. For details about a species&#8217; habitat preferences and local distribution, see the Washington range map and the sections on &#8220;Habitat&#8221; and &#8220;Where to Find in Washington&#8221; in its BirdWeb account.</p>";
        } else {
            text = "<p>The checklist does not include casual and accidental visitors but only those species that occur annually in the ecoregion (&#8220;Irregular&#8221; category excepted). An <em>average</em> status is shown for months during which the abundance of a migratory species increases or decreases. For example, a species that is Rare in the first week of April but becomes Fairly Common by the last week may be shown as Uncommon for that month. While many bird species will be of similar abundance in a given month throughout an ecoregion, some will not, perhaps occurring only in the southern or northern part, or in limited habitats. In such cases, the status is shown for the part of the region where the species is <em>most abundant</em> in that month. For details about a species&#8217; habitat preferences and local distribution, click on its name on the checklist to visit its BirdWeb account (see especially the Washington range map and the sections on &#8220;Where to Find in Washington&#8221; and &#8220;Habitat&#8221;).</p>";
        }
        return text;
    }
</script>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
 
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        Abundance Codes</h1>
    <p>
        Definitions of abundance codes (developed by the American Birding Association):</p>
    <dl>
        <dt>COMMON (C)</dt>
        <dd>
            Found in moderate to large numbers, and easily found in appropriate habitat at the
            right time of year.</dd>
        <dt>FAIRLY COMMON (FC)</dt>
        <dd>
            Found in small to moderate numbers, and usually easy to find in appropriate habitat
            at the right time of year.</dd>
        <dt>UNCOMMON (U)</dt>
        <dd>
            Found in small numbers, and usually&#8212;but not always&#8212;found with some effort
            in appropriate habitat at the right time of year.</dd>
        <dt>RARE (R)</dt>
        <dd>
            Occurs annually in very small numbers. Not to be expected on any given day, but
            may be found with extended effort over the course of the appropriate season(s).</dd>
        <dt>IRREGULAR (I)</dt>
        <dd>
            Represents an irruptive species whose numbers are highly variable from year to year.
            There may be small to even large numbers present in one year, while in another year
            it may be absent altogether.</dd>
        <dt>BLANK</dt>
        <dd>
            Has occurred less than annually (if at all) in very small numbers or at great intervals.</dd>
    </dl>
    <%= Explanation() %>
</asp:Content>
