<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Ecoregions and Birding Sites" %>

<%@ Register TagPrefix="sas" TagName="ecoregionlist" Src="components/EcoregionList.ascx" %>
<%@ Register TagPrefix="sas" TagName="credit" Src="components/credit.ascx" %>
<%@ Register TagPrefix="sas" TagName="about" Src="components/about.ascx" %>
<%@ Import Namespace="BirdWeb" %>
<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <sas:ecoregionlist ID="ecoregionlist" runat="server" LinkType="sites"></sas:ecoregionlist>
    <sas:about ID="about" runat="server" />
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        Browse Ecoregions and Birding Sites
    </h1>
    <p>
        Please select an ecoregion by clicking on the map below or on the list on the left.
    </p>
    <img style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" src="<%= Util.MakeUrl("web_images/master_map_simple.jpg") %>" usemap="#master_map_simple"
        img="img" />
    <map name="master_map_simple">
        <area title="1 Pacific Northwest Coast" shape="POLY" alt="1 Pacific Northwest Coast"
            coords="12,55,12,97,26,131,31,162,42,263,108,278,104,234,98,209,111,195,101,184,83,185,78,171,97,158,121,136,116,103"
            href="<%= Util.MakeUrl("ecoregion/sites/pacific_northwest_coast/site") %>" />
        <area title="2 Puget Trough" shape="POLY" alt="2 Puget Trough" coords="108,280,118,296,123,329,126,335,158,343,157,335,142,329,142,316,134,302,123,301,120,280,118,271,131,268,134,256,141,238,128,227,120,213,134,209,147,219,159,213,165,199,181,190,188,186,186,161,177,153,184,149,200,159,196,123,206,120,197,108,189,103,174,56,163,29,177,9,135,4,144,29,111,42,131,61,143,58,134,77,138,86,125,91,109,83,74,83,115,102,121,137,77,173,85,184,101,183,113,198,98,211,111,265"
            href="<%= Util.MakeUrl("ecoregion/sites/puget_trough/site") %>" />
        <area title="3 North Cascades" shape="POLY" alt="3 North Cascades" coords="176,7,271,9,268,25,273,36,266,45,272,52,265,56,261,53,249,61,249,73,257,88,250,100,241,100,238,118,244,125,241,143,221,163,220,173,199,157,195,123,207,120,188,100,163,26"
            href="<%= Util.MakeUrl("ecoregion/sites/north_cascades/site") %>" />
        <area title="4 West Cascades" shape="POLY" alt="4 West Cascades" coords="158,341,185,328,201,329,191,311,194,289,213,274,209,261,218,259,216,251,220,229,214,216,211,215,218,192,225,185,223,175,182,149,178,153,188,161,193,162,186,169,186,185,165,199,158,213,146,220,129,210,123,214,144,240,132,269,120,273,123,300,133,301,143,318,144,329,159,334"
            href="<%= Util.MakeUrl("ecoregion/sites/west_cascades/site") %>" />
        <area title="5 East Cascades" shape="POLY" alt="5 East Cascades" coords="202,329,221,331,229,337,234,324,245,315,259,320,290,302,266,295,250,262,270,189,299,200,291,156,328,115,313,108,271,51,267,54,260,54,249,61,250,76,256,90,249,100,242,100,240,121,245,129,242,145,220,165,227,184,215,215,222,231,218,252,220,261,211,262,213,277,197,287,189,303"
            href="<%= Util.MakeUrl("ecoregion/sites/east_cascades/site") %>" />
        <area title="6 Okanogan" shape="POLY" alt="6 Okanogan" coords="270,8,436,7,440,31,449,26,454,49,469,58,476,98,486,103,502,103,499,123,502,135,509,135,510,141,493,146,496,160,468,126,462,130,450,128,443,131,433,117,424,128,410,121,403,118,397,120,383,112,379,94,354,84,351,78,341,83,338,108,331,108,326,113,312,106,270,47,267,42,274,36,268,24"
            href="<%= Util.MakeUrl("ecoregion/sites/okanogan/site") %>" />
        <area title="7 Canadian Rockies" shape="POLY" alt="7 Canadian Rockies" coords="437,8,511,8,512,187,496,157,495,145,510,140,508,133,501,133,502,104,496,98,482,100,474,91,471,58,453,47,449,23,440,29"
            href="<%= Util.MakeUrl("ecoregion/sites/canadian_rockies/site") %>" />
        <area title="8 Blue Mountains" shape="POLY" alt="8 Blue Mountains" coords="435,305,522,307,517,298,520,288,511,269,512,292,490,292,497,284,498,272,484,274,471,267,467,279,452,279"
            href="<%= Util.MakeUrl("ecoregion/sites/blue_mountains/site") %>" />
        <area title="9 Columbia Plateau" shape="POLY" alt="9 Columbia Plateau" coords="229,341,235,324,241,322,247,317,259,321,292,301,268,296,252,262,272,190,299,201,293,159,329,111,339,107,343,83,350,80,353,87,379,96,379,112,396,121,404,117,425,128,433,117,442,132,451,128,464,131,469,127,492,158,510,186,512,270,510,288,489,291,498,282,498,272,484,272,470,265,465,278,452,277,432,300,432,305,382,303,370,311,355,309,336,310,323,317,277,332,270,325"
            href="<%= Util.MakeUrl("ecoregion/sites/columbia_plateau/site") %>" />
        <area title="0 Oceanic" shape="POLY" alt="0 Oceanic" coords="135,0,74,0,97,21,101,55,89,60,81,67,69,63,0,29,0,344,38,345,45,272,42,261,24,134,9,97,10,50,71,81,112,81,124,87,132,82,130,61,107,41,140,26"
            href="<%= Util.MakeUrl("ecoregion/sites/oceanic/site") %>" />
        <area shape="RECT" alt="" coords="0,0,0,0" href="sites" />
    </map>
    <sas:credit ID="image_credit" runat="server" ContributorID="360" CreditType="M"></sas:credit>
</asp:Content>
