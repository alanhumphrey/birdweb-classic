<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Ecoregions and Birding Sites" %>

<%@ Register TagPrefix="sas" TagName="sitelist" Src="components/SiteList.ascx" %>
<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="image" Src="components/image.ascx" %>
<%@ Register TagPrefix="sas" TagName="credit" Src="components/credit.ascx" %>
<%@ Register TagPrefix="sas" TagName="bwabout" Src="components/about.ascx" %>

<script runat="server">

    string site_name;
    public string SiteName {
        set { site_name = value; }
        get { return site_name; }
    }

    void Page_Load( object Source, EventArgs e ) {
        if ( Request.QueryString["id"] != null ) {
            string site_name = BirdWeb.SiteName( Request.QueryString["id"] );
            string site_url = "site/" + site_name;
            if ( Request.QueryString["ecoregion"] != null ) {
                site_url += "/" + Request.QueryString["ecoregion"];
            }
            Response.RedirectPermanent( site_url, true );
        }
        string site_id = BirdWeb.SiteIDFromName( Request.QueryString["name"] );
        if ( Request.QueryString["ecoregion"] != null ) {
            Detail.DataSource = BirdWeb.BirdingSiteDetail( site_id, Request.QueryString["ecoregion"] );
            SideDetail.DataSource = BirdWebDB.BirdingSiteDetail( site_id, Request.QueryString["ecoregion"] );
        } else {
            Detail.DataSource = BirdWeb.BirdingSiteDetail( site_id );
            SideDetail.DataSource = BirdWebDB.BirdingSiteDetail( site_id );
        }
        Detail.DataBind();
        SideDetail.DataBind();
        Sources.DataSource = BirdWeb.BirdingSiteSources( Request.QueryString["id"] );
        Sources.DataBind();
        
        HtmlMeta description = new HtmlMeta();
        HtmlHead head = ( HtmlHead ) Page.Header;
        description.Name = "Description";
        description.Content = "Description of birding site.";
        head.Controls.Add( description );
        head.Title = SiteName;
    }

    string FormatReference( string title, string pages, string url, string status ) {

        string listing = "";

        if ( url != "No data at this time." ) {
            if ( status == "200" ) {
                listing = string.Format( "<li><a href='{0}'>{1}</a>", url, title );
            }
        } else {
            listing = "<li>" + title;
        }

        if ( listing.Length > 0 && pages != "No data at this time." && pages.Length > 0 ) {
            listing = string.Format( "{0}, {1}", listing, pages );
        }

        if ( listing.Length > 0 ) {
            listing = string.Format( "{0}</li>", listing );
        }
        return listing;
    }

</script>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <h2 class="qlink">
        Site Details
    </h2>
    <ul>
        <li><a href="#site">The Site</a> </li>
        <li><a href="#birds">The Birds</a> </li>
        <li><a href="#directions">Directions and Suggestions</a> </li>
        <li><a href="#references">References</a> </li>
    </ul>
    <sas:db ID="BirdWebDB" runat="server"></sas:db>
    <asp:Repeater ID="SideDetail" runat="server">
        <ItemTemplate>
            <sas:sitelist ID="NavSiteList" SiteID='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) %>'
                Ecoregion='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "ecoregionid" ) ) %>'
                Location="SideNav" runat="server" />
        </ItemTemplate>
    </asp:Repeater>

    <sas:bwabout ID="about" runat="server"></sas:bwabout>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <sas:db ID="BirdWeb" runat="server"></sas:db>
    <asp:Repeater ID="Detail" runat="server">
        <ItemTemplate>
            <h1>
                <%# SiteName = BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "site_name" ) ) %>
            </h1>
            <sas:credit ID="credit" runat="server" ContributorID='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "contributor_id" ) ) %>' CreditType="S"> 
            </sas:credit>
            <sas:image ID="imageCtrl" runat="server" ImageType='S' CreditType='P' ImageID='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "default_site_image_id" ) ) %>' />
            </p>
            <h2 class="details">
                <a name="site"></a>The Site</h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "site_description" ) ) %>
            </p>
            
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <h2 class="details">
                <a name="birds"></a>The Birds
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "site_birds" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <h2 class="details">
                <a name="directions"></a>Directions and Suggestions
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "site_notes" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
        </ItemTemplate>
    </asp:Repeater>
    <h2 class="details">
        <a name="references"></a>References
    </h2>
    <asp:Repeater ID="Sources" runat="server">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <%# FormatReference(BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "title" ) ),
                                            BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "pages" ) ),
                                            BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "url" ) ),
                                            BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "last_status" ) )  ) %>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
    <p class="top">
        <a href="#">back to top</a>
    </p>
</asp:Content>
