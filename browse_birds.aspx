<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Browse Birds" %>
<%@ Register TagPrefix="sas" TagName="orders" Src="components/orders.ascx" %>
<%@ Register TagPrefix="sas" TagName="orderlist" Src="components/OrderList.ascx" %>
<%@ OutputCache Duration="3600" VaryByParam="none"  %>
<asp:Content ID="sidenavcontent" ContentPlaceHolderID="sidenav" runat="server">
                    <sas:orders id="ordersCtrl" runat="server"></sas:orders>
</asp:Content>
<asp:Content ID="maincontent" ContentPlaceHolderID="content" runat="server">
                    <h1>Browse Birds
                    </h1>
                    <p>
                        BirdWeb allows you to search for a specific bird by entering its name in the search
                        box (above left), or to look for birds according to their taxonomic grouping. Washington's
                        birds belong to 18 <em>orders</em>&mdash;large groupings of related families and species.
                        To learn about an order and its Washington representatives, select a group from the
                        list on the left or scroll down the page.
                    </p>
                    <sas:orderlist id="OrderList" runat="server"></sas:orderlist>

</asp:Content>

