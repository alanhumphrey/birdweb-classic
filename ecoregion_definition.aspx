<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - Ecoregion Definition" %>

<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        What is an Ecoregion?</h1>
    <p>
        Washington is a large and diverse state, with wet and dry forests, prairies and
        sagebrush steppe, high country and low, lakes and rivers, and a long coastline.
        It also has many areas modified to accommodate human populations such as cities
        and towns and farms and ranches. Many birds are widely distributed, while others
        are found principally or exclusively in one or several of its ten ecoregions.
    </p>
    <blockquote>
        <p>
            "Ecoregions reflect broad ecological patterns occurring on the landscape. In general,
            each ecoregion has a distinctive composition and pattern of plant and animal species
            distribution. Abiotic factors, such as climate, landform, soil, and hydrology are
            important in the development of ecosystems, and thus help define ecoregions. Within
            an individual ecoregion, the ecological relationships between species and their
            physical environment are essentially similar.
        </p>
        <p>
            "Because of this, using ecoregions to view the distribution of species and ecosystems
            across the landscape has emerged as a useful tool. Ecoregions make biological sense,
            compared to using politically derived lines, such as county, state or national boundaries.
            They also provide an ecological basis for partitioning the state into subunits for
            conservation planning purposes." *
        </p>
    </blockquote>
    <p> BirdWeb has adopted the ecoregional boundaries defined by the U.S. Environmental
    Protection Agency, with minor modifications by scientists of the Natural Heritage
    Program of the Washington State Department of Natural Resources and their planning
    partners, including The Nature Conservancy and the Washington Department of Fish
    and Wildlife. Portions of nine terrestrial ecoregions occur within Washington's
    borders. BirdWeb has added a tenth, Oceanic ecoregion for waters offshore in the
    Pacific. </p>
    <br />
    <br />
    <p>
        <cite>* State of Washington Natural Heritage Plan.</cite> 2003.<br />
        Olympia,Washington State Department of Natural Resources.
    </p>
</asp:Content>
