<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - Audio Sources" %>

<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string audiosourceslist = "";
    public string AudioSourcesList {
        get { return audiosourceslist; }
        set { audiosourceslist = value; }
    }

    void Page_Load( object Source, EventArgs e ) {

        OdbcDataReader reader = BirdWebDB.AudioSources();
        StringBuilder checklist = new StringBuilder();
        checklist.Append( "<table id='concern' width='100%'>" );
        checklist.Append( "<tr><th>Bird</th><th>Recordist</th><th>Contributed By</th></tr>" );

        bool altrow = false;
        while ( reader.Read() ) {
            checklist.Append( "<tr" + ( altrow ? " class=altrow>" : ">" ) );
            altrow = !altrow;

            checklist.Append( String.Format( "<th class='birdname'><a href='bird/{0}'>{1}</a></th>", Util.EncodeName( reader[1].ToString()), reader[1].ToString() ) );
            checklist.Append( "<td>" + reader[2].ToString() + "</td>" );
            checklist.Append( "<td>" + reader[3].ToString() + "</td>" );
            checklist.Append( "</tr>" );
        }
        reader.Close();
        checklist.Append( "</table>" );
        AudioSourcesList = checklist.ToString();

    }

</script>

<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <sas:db ID="BirdWebDB" runat="server"></sas:db>
    <h1>
        Bird Audio Sources
    </h1>
    <p>
        <a href="http://macaulaylibrary.org/">The Macaulay Library of Natural
            Sounds</a>, Cornell Laboratory of Ornithology, Ithaca, New York. Individual
        recordist credits listed below.
    </p>
    <p>
        Peter Ward and Ken Hall, from their CD <i>Songbirds of the Northwest: Nature Sounds
            of British Columbia Washington and Oregon</i>.
    </p>
    <h2>
        Recordist Credits
    </h2>
    <%= AudioSourcesList%>
</asp:Content>
