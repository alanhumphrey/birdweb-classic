<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birdweb - About" %>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <div class="qlink">
        Additional Information
    </div>
    <ul>
        <li><a href="resources">Resources</a> </li>
        <li><a href="acknowledgments">Credits</a> </li>
    </ul>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <h1>
        About BirdWeb
    </h1>
    <p>
        In late 2002, Seattle Audubon launched BirdWeb, an easy-to-use on-line resource
        designed to help residents and visitors alike learn more about the birds of Washington.
        BirdWeb offers extensive profiles of the 350-plus species of birds that occur regularly
        in Washington, along with shorter notes on more than 100 species of rarities. For
        each bird species, users will find color photographs, range maps, and information
        about the bird's habitat, behavior, diet, nesting, distribution, and conservation
        status.
    </p>
    <p>
        BirdWeb was immediately successful and attracted widespread praise. "I applaud Seattle
        Audubon's innovative leadership in developing BirdWeb," said Kenn Kaufman, author
        of <i>Lives of North American Birds, The Peterson Guide to Advanced Birding</i>,
        and <i>Birds of North America</i>. "This is a terrific resource: accurate, comprehensive,
        compiled by experts, and based on the very latest information. It should accomplish
        great things in getting the public more involved with birds and with their conservation."
    </p>
    <p>
        In 2004, Seattle Audubon moved ahead with a second phase of BirdWeb, revamping the
        design and content of the original web site and adding reference sections on Washington's
        ten ecoregions and their birdlife; descriptions of and directions to numerous representative
        birding locations for each ecoregion; more and better photographs; and sound recordings
        of bird vocalizations. The greatly expanded BirdWeb was launched in late 2005. Additions
        and updates will be made on an ongoing basis.
    </p>
    <p>
        BirdWeb aims to connect bird watchers with the information they need to enjoy, understand,
        and protect Washington's birds and the habitats they depend on for survival. "One-third
        of Washington's birds (123 species) are listed as species of special concern, and
        Seattle Audubon is working to protect them," said Emily Sprong, the first BirdWeb
        project lead. "We believe BirdWeb is an important tool for fostering an appreciation
        of our state's birds that will ultimately lead to increased citizen involvement
        in bird conservation and environmental education."
    </p>
    <p>
        Thank you for visiting BirdWeb. Your comments and suggestions are welcome and will
        be given full consideration. Please address them to <a href="mailto:birdweb@seattleaudubon.org">
            birdweb@seattleaudubon.org</a>.
    </p>
</asp:Content>
