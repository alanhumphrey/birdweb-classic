<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    // left nav listing
    
    int listRowCount = 0;
    public int ListRowCount {
        get { return listRowCount++; }
    }
    
    string linkType = "internal";
    public string LinkType {
        get { return linkType; }
        set { linkType = value; }
    }
    
    bool mapNumbers = true;
    public bool MapNumbers {
        get { return mapNumbers; }
        set { mapNumbers = value; }
    }
    
    bool showCurrentEcoregion = true;
    public bool ShowCurrentEcoregion {
        get { return showCurrentEcoregion; }
        set { showCurrentEcoregion = value; }
    }
    
    string ecoregionID = "";
    public string EcoregionID {
        get { return ecoregionID; }
        set { ecoregionID = value; }
    }
    
    void Page_Load(object Source, EventArgs e) {
        List.DataSource = BirdWeb.EcoregionSummary();
        List.DataBind();
    }
    
    string Listing (string regionName, string ecoregionID) {
        int regionNumber = ListRowCount;
        string link = "";
        if ( ShowCurrentEcoregion || ecoregionID != EcoregionID ) {
            if ( LinkType == "internal") {
                link = string.Format("<a href='#{0}'>{1} {2}</a>", regionNumber, regionNumber, regionName);
            } else if ( LinkType.Equals("sites") ) {
                if ( MapNumbers ) {
                    string url = string.Format("ecoregion/sites/{0}/site", Util.EncodeName(regionName));
                    link = string.Format( "<a href='{0}'>{1} {2}</a>", Util.MakeUrl(url), regionNumber, regionName );
                } else {
                    string url = string.Format("ecoregion/sites/{0}/site", Util.EncodeName(  regionName)) ;
                    link = string.Format( "<a href='{0}'>{1}</a>", Util.MakeUrl(url), regionName );
                }
            } /*else {
                if ( MapNumbers ) {
                    link = string.Format("<a href='ecoregion_details.aspx?id={0}'>{1} {2}</a>", ecoregionID, regionNumber, regionName);
                } else {
                    link = string.Format("<a href='ecoregion_details.aspx?id={0}'>{1}</a>", ecoregionID, regionName);
                }
            }*/
        }
        if ( link.Length > 0 ) {
            link = string.Format("<li>{0}</li>", link);
        }
        return link;
    
    }
    
    string heading () {
        if ( ! MapNumbers ) {
            return "Other Ecoregions";
        } else {
            return "Ecoregions";
        }
    }

</script>
<sas:DB id="BirdWeb" runat="server"></sas:DB>
<h2 class="qlink"><%= heading() %>
</h2>
<asp:Repeater id="List" runat="server">
    <HeaderTemplate>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <%# Listing( BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "ecoregion_name" ) ), BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) )  %>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
