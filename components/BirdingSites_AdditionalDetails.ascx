<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<script runat="server">

    string site_id ;
    public string SiteID {
        set {site_id = value;}
        get {return site_id;}
    }

    void Page_Load(object Source, EventArgs e) {
        SiteAttributes.DataSource = BirdWeb.SiteAttributes( SiteID);
        SiteAttributes.DataBind();
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<asp:Repeater id="SiteAttributes" runat="Server">
    <ItemTemplate>
        <h2 class="details"><%# DataBinder.Eval( Container.DataItem, "attribute_name" ) %>
        </h2>
    </ItemTemplate>
</asp:Repeater>
