<%@ Control Language="C#" %>
<script runat="server">

    private string soundFile;
    public string SoundFile {
        set { soundFile = value; }
        get { return "mms://" + Page.Request.Url.Host + Page.Request.ApplicationPath + "/" + soundFile; }

    }

</script>
<!-- Insert content here -->

<% Response.Write( SoundFile ); %><br/>
<OBJECT
    ID="MediaPlayer"
	classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95"
	standby="Loading Microsoft Windows Media Player components..."
	TYPE="application/x-oleobject">
	<PARAM name="FileName" VALUE="<% Response.Write( SoundFile ); %>">
	<param name="AutoStart" value="False">
	<param name="ShowControls" value="True">
	<param name="ShowStatusBar" value="True">
	<param name="ShowDisplay" value="False">
	<param name="AutoRewind" value="True">

		<embed TYPE="application/x-mplayer2"
		  pluginspage="http://www.microsoft.com/Windows/Downloads/Contents/MediaPlayer/"
		  Name=MediaPlayer
		  FileName="<% Response.Write( SoundFile ); %>"
		  ShowControls=False
		  ShowDisplay=False
		  ShowStatusBar=False
		  Autostart=False >
		</embed>
</OBJECT>
