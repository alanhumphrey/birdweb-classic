<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="credit" Src="credit.ascx" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string imageID;
    public string ImageID {
        get { return imageID; }
        set { imageID = value; }
    }

    private string birdID;
    public string BirdID {
        get { return birdID; }
        set { birdID = value; }
    }

    private string imageType = "P";
    public string ImageType {
        get { return imageType; }
        set { imageType = value.ToUpper(); }
    }

    public string ImageClass {
        get {
            if (imageType.Equals("M")) {
                return "mapimage";
            } else {
                return "";
            }
        }
    }

    private Boolean link_to_larger_image = false;
    public Boolean LinkToLargerImage {
        get { return link_to_larger_image; }
        set { link_to_larger_image = value; }
    }

    private void image_ItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            DbDataRecord row = e.Item.DataItem as DbDataRecord;
            string image = string.Format("<img src='{0}' width='{1}' height='{2}' title=\"{3}\" alt=\"{3}\"/>", Util.MakeUrl(row["image_file"].ToString()), row["width"].ToString(), row["height"].ToString(), row["image_description"].ToString());
            if (LinkToLargerImage) {
                string url = Util.MakeUrl(string.Format("bigger_image.aspx?id={0}&type=p", row["larger_image_id"]));
                image = string.Format("<a href='#' data-width='{0}' data-height='{1}' data-image_url='{2}'>{3}</a>",
                    row["larger_width"], row["larger_height"], url, image);
            } else {
                if (ImageType.Equals("P")) {
                    image = string.Format("{0}<p class='enlarged_description'>{1}</p>", image, row["image_description"]);
                }
            }
            ImageHtml = image;
        }
    }

    private string image_html = "";
    private string ImageHtml {
        get { return image_html; }
        set { image_html = value; }
    }

    private string PictureGallery {
        get {
            if (!ImageType.Equals("B")) {
                return "";
            }

            using (OdbcDataReader reader = BirdWeb.BirdPictures(BirdID)) {
                if (reader.HasRows) {
                    StringBuilder pictures = new StringBuilder();
                    pictures.Append("<div id='filmstripscroll'><div id='filmstripholder'><ul id='filmstrip'>");

                    while (reader.Read()) {
                        pictures.Append("<li class='birdgallery'>");
                        pictures.Append(string.Format("<a href='#' data-image_url='{5}' data-width='{6}' data-height='{7}'><img src=\"{0}\" width='{1}' height='{2}' title=\"{3}\" alt=\"{3}\" data-large_file='{4}' data-contributor='{8}' data-contributor_url='{9}'/></a>",
                            Util.MakeUrl(reader["image_file"].ToString()), reader["small_width"], reader["small_height"], reader["image_description"], Util.MakeUrl(reader["large_image_file"].ToString()),
                            Util.MakeUrl(string.Format("bigger_image.aspx?id={0}&type=p", reader["xl_id"])), reader["xl_width"], reader["xl_height"], reader["contributor_name"], reader["url"]));
                        pictures.Append("</li>");
                    }
                    pictures.Append("</ul></div></div>");
                    pictures.Append("<div id='slider'></div>");
                    pictures.Append("<p>Hover over to view. Click to enlarge.</p>");
                    return pictures.ToString();
                } else {
                    return "";
                }
            }
        }
    }

    void Page_Load(object Source, EventArgs e) {

        if (ImageType.Equals("B")) {
            image.DataSource = BirdWeb.DefaultBirdPicture(ImageID);
        } else {
            image.DataSource = BirdWeb.ImageInfo(ImageID);
        }
        image.DataBind();
    }

</script>
<sas:db ID="BirdWeb" runat="server"></sas:db>
<div class="photo <%# ImageClass %>">
    <asp:Repeater ID="image" runat="server" OnItemDataBound="image_ItemDataBound">
        <ItemTemplate>
            <figure>
                <%= ImageHtml %>
                <figcaption>
                    <sas:credit ID="image_credit" runat="server" ContributorID='<%# Eval("contributor_id") %>'
                        CreditType="<%# ImageType %>"></sas:credit>
                </figcaption>
            </figure>
        </ItemTemplate>
    </asp:Repeater>
    <%= PictureGallery %>
</div>
