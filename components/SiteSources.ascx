<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="credit" Src="credit.ascx" %>
// user control to display sources for birding sites.
<script runat="server">

    // Insert user control code here
    //

    private string siteID;
    public string SiteID {
        get { return siteID ; }
        set { siteID = value; }
    }

    void Page_Load(object Source, EventArgs e) {

       // site_sources.DataSource=BirdWeb.SiteSources(SiteID);
       // site_sources.DataBind();
    }

    string ListItem (object title, object url, object pages) {
        if ( url.ToString().Length > 0 ) {
            return String.Format("<a href='{0}'>{1} {2}</a>", url.ToString(), title.ToString(), pages.ToString());
        } else {
            return title.ToString() + " " + pages.ToString();
        }
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<asp:Repeater id="site_sources" runat="server">
    <HeaderTemplate>
       <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><%# ListItem( DataBinder.Eval( Container.DataItem, "Title"),
                          DataBinder.Eval( Container.DataItem, "url"),
                          DataBinder.Eval( Container.DataItem, "Pages") ) %> </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
