<%@ Control Language="C#" Debug="true" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<h2 class='qlink'>About</h2>
<ul>
    <li><a href="<%= Util.MakeUrl("ecoregiondefinition")%>">Ecoregions</a> </li>
    <li><a href="<%= Util.MakeUrl("abundancecode/ecoregion")%>">Bird Checklists</a> </li>
    <li><a href="<%= Util.MakeUrl("aboutbirdingsites")%>">Birding Sites</a> </li>
    <li><a href="<%= Util.MakeUrl("resources")%>">Birding Resources</a> </li>
</ul>
