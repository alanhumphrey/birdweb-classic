<%@ Control Language="C#" debug="true"%>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    // find and display all species from the same family
    // separate out the rarities

    string bird_id ;
    public string BirdID {
        set { bird_id = value; }
        get { return bird_id; }
    }

    string family_id;
    public string FamilyID {
        set { family_id = value; }
        get { return family_id; }
    }

    string title = "Family Members";
    public string Title {
        set { title = value; }
        get { return title; }
    }

    private bool show_image = false;
    public bool ShowImage {
        get { return show_image; }
        set { show_image = value; }
    }
    
    private string Url(string bird_name) {
        return Util.MakeUrl("bird/" + Util.EncodeName(bird_name));
    }

    private string ImageFile(string image_filename, string image_description) {
        string image = string.Format("<img src='{0}' alt='{1}' title='{1}'/>", Util.MakeUrl(image_filename), image_description); 
        if (ShowImage) {
            return image;
        } else {
            return string.Format("<span class='popupimage'>{0}</span>", image);
        }
    }

    private bool show_scientific_name = false;
    public bool ShowScientificName {
        get { return show_scientific_name; }
        set { show_scientific_name = value; }
    }

    private string ScientificName(string genus, string species) {
        if (ShowScientificName) {
            return string.Format("<span class='latinname'>{0} {1}</span>", genus, species);
        } else {
            return "";
        }
    }

    private string current_bird_css_class = "";
    public string CurrentBirdCssClass {
        get { return current_bird_css_class; }
        set { current_bird_css_class = value; }
    }
    private string CommonName(string bird_name, string bird_id) {
        if (CurrentBirdCssClass.Length > 0 && bird_id.Equals(BirdID)) {
            return string.Format("<span class='{0}'>{1}</span>", CurrentBirdCssClass, bird_name);
        } else {
            return string.Format("<span>{0}</span>", bird_name);
        }
    }

    private string list_class = "";
    public string ListCssClass {
        set { list_class = value; }
        get {
            if (list_class.Length > 0) {
                return string.Format(" class='{0}'", list_class);
            } else {
                return "";
            }
        }
    }
    
    private string BirdList {
        get {
            StringBuilder result = new StringBuilder();
            OdbcDataReader reader = null;

            try {
                if (BirdID != null) {
                    reader = BirdWeb.SpeciesFamily(BirdID);
                } else if (FamilyID != null) {
                    reader = BirdWeb.BirdsInFamily(FamilyID);
                } else {
                    throw new ArgumentNullException("Must provide either a BirdID or a FamilyID.", "BirdID or FamilyID");
                }
                result.Append("<div class='familymembers'>");
                result.Append("<h2>" + Title + "</h2>");
                result.Append("<ul>");
                while (reader.Read()) {
                    result.Append(string.Format("<li{0}><a class='thumbnail' href='{1}'>{2}{3}{4}</a></li>",
                            ListCssClass,
                            Url(reader["common_name"].ToString()),
                            ImageFile(reader["image_file"].ToString(), reader["image_description"].ToString()),
                            CommonName(reader["common_name"].ToString(), reader["id"].ToString()),
                            ScientificName(reader["genus_scientific_name"].ToString(), reader["species_scientific_name"].ToString())
                        ));
                }
                result.Append("</ul>");
                result.Append("</div>");
            } catch {
                //
            } finally {
                reader.Close();
            }
            return result.ToString();
        }
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<%= BirdList %>
