<%@ Control Language="C#" Debug="true" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string location = "";
    public string Location {
        get { return location; }
        set { location = value; }
    }

    private string css_class = "";
    private string CSSClass {
        get { return css_class; }
        set { css_class = value; }
    }

    private string header_image = "get_involved_homepage_banner.jpg";
    private string HeaderImage {
        set { header_image = value; }
        get { return header_image; }
    }

    private string SASResourceList {
        get { 
            StringBuilder resources = new StringBuilder();
            resources.Append("<ul>");
            resources.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Go Birding!</a></li>", "https://birdsconnectsea.org/get-involved/go-birding/"));
            resources.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Volunteer</a></li>", "https://birdsconnectsea.org/get-involved/volunteer/"));
            resources.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Adult Classes</a></li>", "https://birdsconnectsea.org/learn/classes/"));
            resources.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">{1}Nature Camp</a></li>", "https://birdsconnectsea.org/learn/youth-programs/nature-camp/", Location.Equals("homepage") ? "Summer " : "" ));
            resources.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">{1}Nature Shop</a></li>", "https://birdsconnectsea.org/the-nature-shop/", Location.Equals("homepage") ? "Online " : "" ));
            resources.Append("</ul>");
            return resources.ToString();
        }
    }

    private string SASSupport {
        get {
            return string.Format("<div style='text-align:center' ><a href=\"{0}\"  target=\"_blank\" id=\"support_sas\" class=\"{1}\">Support<br/>Birds Connect Seattle</a></div>", "https://birdsconnectsea.org/donate/", CSSClass);
        }
    }
    
    private string SASPromo {
        get {
            StringBuilder promo = new StringBuilder();
            if (Location.Equals("homepage")) {
                promo.Append("<h2><span>&nbsp;&nbsp;Get Involved &nbsp;&nbsp;</span></h2>");
                promo.Append(string.Format("<a href='https://birdsconnectsea.org' target='_blank'><img src='{0}' alt='birders with binoculars' width='100' height='100' /></a>", Util.MakeUrl("web_images/" + HeaderImage)));
                promo.Append("<h3>Birds Connect Seattle has numerous resources available to you.</h3>");
                promo.Append(SASResourceList);
                promo.Append(SASSupport);
            } else {
                promo.Append(SASSupport);
                promo.Append(string.Format("<aside class=\"sas_promo {0}\"><header>", CSSClass));
                promo.Append(string.Format("<img src='{0}' alt='birders with binoculars' /></header><article>", Util.MakeUrl("web_images/" + HeaderImage)));
                promo.Append("<h3>Get Involved With<br />Birds Connect Seattle!</h3>");
                promo.Append(SASResourceList);
                promo.Append("<footer></footer>");
            }
            return promo.ToString();
        }
    }

    void Page_Load(object Source, EventArgs e) {
        if (Location.ToLower().Equals("leftnav")) {
            HeaderImage = "get_involved_leftnav_banner.jpg";
            CSSClass = "";
        }
    }

</script>
<%= SASPromo %>