<%@ Control Language="C#" Debug="true" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ import Namespace="System.Data.Odbc" %>
<script runat="server">

    private string creditType = "";
    public string CreditType {
        get {
                if (creditType.ToUpper() == "P" ) {
                    return "Photo courtesy of ";
                } else if (creditType.ToUpper() == "M" ) {
                    return "Map courtesy of ";
                } else {
                    return "&copy; ";
                }
        }
        set { creditType = value; }
    }
    
    private string contributorID = "";
    public string ContributorID {
        set { contributorID = value; }
        get { return contributorID ;}
    }
    
    private string contributorName;
    public string ContributorName {
        get {
            string name;
            if ( attribution.Length > 0 ) {
                name = attribution;
            } else {
                name = contributorName;
            }
    
            if ( url.Length > 0 ) {
                return "<a href='" + url + "'>" + name + "</a>";
            } else {
                return name;
            }
        }
    }
    
    public string attribution;
    private string url;
    private string courtesyString;
    
    void Page_Load(object Source, EventArgs e) {
    
        if ( ContributorID != "" && ContributorID != null) {
            OdbcDataReader reader = BirdWeb.ContributorInfo(ContributorID);
            reader.Read() ;
    
            contributorName = reader[0].ToString();
            attribution = reader[1].ToString();
            url = reader[2].ToString();
            reader.Close();
            courtesyString=(contributorName !="Not Yet Available")? CreditType + ContributorName: "";
        }
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<div class="credit"><span><%= courtesyString %></span>
</div>