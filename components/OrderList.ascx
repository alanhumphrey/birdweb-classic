<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="families" Src="FamilyGroup.ascx" %>
<script runat="server">

    void Page_Load(object Source, EventArgs e) {

        OrderList.DataSource = BirdWeb.BirdOrders();
        OrderList.DataBind();
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<div class="orderlist">
    <asp:Repeater id="OrderList" runat="Server">
        <HeaderTemplate>
            <dl>
        </HeaderTemplate>
        <ItemTemplate>
            <dt><a name="<%# DataBinder.Eval( Container.DataItem, "order_navigation_name" ) %>"></a><%# DataBinder.Eval( Container.DataItem, "order_navigation_name" ) %> (Order <%# DataBinder.Eval( Container.DataItem, "order_scientific_name" ) %>) </dt> 
            <dd>
                <%# BirdWeb.getValue(DataBinder.Eval( Container.DataItem, "order_description" )) %>
            </dd>
            <dd>
                <sas:families id="FamilyList" runat="server" OrderID='<%# DataBinder.Eval( Container.DataItem, "id" ) %>'></sas:families>
            </dd>
            <dd>
                <p class='top'>
                    <a href='#'>back to top</a>
                </p>
            </dd>
        </ItemTemplate>
        <FooterTemplate>
            </dl>
        </FooterTemplate>
    </asp:Repeater>
</div>
