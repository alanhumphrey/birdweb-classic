<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    // this control displays a list of birding sites.  It is used in three places:
    //    1) in the body of the ecoregion and birding sites details pages
    //    2) in the left nav of the ecoregion detail page
    //    3) in the left nav of the birding site detail page.
    //
    //  rules for each section are explained in the code.
    //
         string location = "body";
         public string Location {
             set { location = value; }
             get { return location; }
         }

         string ecoregion;
         public string Ecoregion {
             set { ecoregion = value;}
             get { return ecoregion ;}
         }

         string siteList = "";
         public string SiteList {
             set { siteList = value;}
             get { return siteList ;}
         }

         string siteID = "-1";
         public string SiteID {
             set { siteID = value;}
             get { return siteID ;}
         }


         string Header ( string ecoregion_name ) {
             if ( Location.Equals("body") ) {
                 return "<div id='sitelist'><h2><a name='sites'></a>Birding Sites</h2><ul>";
             }
             if ( !SiteID.Equals( "-1") ){
                 // heading for site detail page left nav
                 return string.Format( "<div class='qlink'>More {0} Sites</div><ul>", ecoregion_name);
             } 
             // heading for left nav
             return string.Format( "<div class='qlink'>{0}</div><ul>", "Birding Sites");
             
         }

         string Footer () {

             if ( Location.Equals("body") ) {
                 // close off the div
                 return "</ul></div>";
             } else {
                 return "</ul>";
             }
         }

         void Page_Load(object Source, EventArgs e) {

                 try {
                     //issue 513 - special case for Pelagic Trips - the
                     // only birding site for ecoregion 0
                     if (SiteID.Equals("69") && !Location.Equals("body")){
                         SiteList = "";
                         return;
                     }
                     string order = "";
                     if ( Location.Equals("body") ) {
                        order = "map"; // make the sort by map number
                     }
                     OdbcDataReader reader = BirdWeb.SitesForEcoregion( Ecoregion, order) ;
                     StringBuilder list = new StringBuilder();
                     if ( reader.HasRows ) {
                         reader.Read() ;
                         list.Append( Header( reader["ecoregion_name"].ToString() ) );
                         do {
                             // don't duplicate the site in the list.  This is used when the list appears
                             // in the left nav of the site detail page.
                             if ( reader["id"].ToString() != SiteID ) {
                                 if ( Location.Equals("body") ) {
                                     // only show the map number in the body of the page.  The list is displayed next
                                     // to a map that has the map numbers.
                                     list.Append( string.Format( "<li>{0} {1}</li>", reader["map_number"], SiteLink( reader["site_name"].ToString(), reader["id"].ToString() ) ) );
                                 } else {
                                     list.Append( string.Format( "<li>{0}</li>", SiteLink( reader["site_name"].ToString(), reader["id"].ToString() ) ) );
                                 }
                             }
                         } while ( reader.Read() );
                         reader.Close();
                         list.Append( Footer() );
                     } else {
                         list.Append("<p>This ecoregion does not have any birding sites.</p>");
                     }
                     SiteList = list.ToString();
                 } catch {
                     Response.Write("error</br>");
                 }
                     
         }

         string SiteLink( string siteName, string siteId) {
             string url = string.Format( "site/{0}/{1}", Util.EncodeName( siteName ), Ecoregion );
             return string.Format( "<a href='{0}'>{1}</a>", Util.MakeUrl(url), siteName );
         }

</script>
<sas:DB id="BirdWeb" runat="server"></sas:DB>
<%= SiteList %>
