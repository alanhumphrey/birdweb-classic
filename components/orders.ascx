<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<script runat="server">

    void Page_Load(object Source, EventArgs e) {
        orderlist.DataSource = BirdWeb.BirdOrders();
        orderlist.DataBind();
    }

    string MenuItem ( object name ) {
        return String.Format("<a href='#{0}'>{0}</a>",(string) name);
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<asp:Repeater id="orderlist" runat="server">
    <HeaderTemplate>
        <h2 class="qlink">Orders
        </h2>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <%# MenuItem( DataBinder.Eval( Container.DataItem, "order_navigation_name" ) ) %>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
