<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string birdID;
    public string BirdID {
        get { return birdID ; }
        set { birdID = value; }
    }
    
    private string birdchecklist;
    public string BirdCheckList {
        get { return birdchecklist ; }
        set { birdchecklist = value; }
    }
    
    void Page_Load(object Source, EventArgs e) {
    
        OdbcDataReader reader = BirdWeb.BirdCheckList(BirdID) ;
    
        if ( reader.HasRows ) {
            StringBuilder checklist = new StringBuilder();
            checklist.Append( "<div id='concern'><h2 class='details' id='abundance'><a name='checklist'></a> <span class='no_print'><a href='" + Util.MakeUrl("abundancecode/bird_detail") + "' target='_blank'><img id='info' src='" + Util.MakeUrl("web_images/information.png") + "' title='Abundance Code Definitions' alt='Abundance Code Definitions'/></a></span>Abundance</h2>" );
            checklist.Append( "<table>" );
            checklist.Append("<caption>C=Common; F=Fairly Common; U=Uncommon; R=Rare; I=Irregular</caption>");
            checklist.Append("<tr><th>Ecoregion</th><th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>May</th><th>Jun</th><th>Jul</th><th>Aug</th><th>Sep</th><th>Oct</th><th>Nov</th><th>Dec</th></tr>");
    
            bool altrow = false;
            while ( reader.Read() ) {
    
                checklist.Append("<tr" + (altrow ? " class=altrow>" : ">" ) );
                altrow = ! altrow;
                string url = string.Format( "ecoregion/{0}", Util.EncodeName( reader["ecoregion_name"].ToString() ));
                checklist.Append( String.Format( "<th class='birdname'><a href='{0}'>{1}</a></th>", Util.MakeUrl( url), reader["ecoregion_name"].ToString() ) );
                checklist.Append("<td>" + reader[2].ToString() + "</td>");
                checklist.Append("<td>" + reader[3].ToString() + "</td>");
                checklist.Append("<td>" + reader[4].ToString() + "</td>");
                checklist.Append("<td>" + reader[5].ToString() + "</td>");
                checklist.Append("<td>" + reader[6].ToString() + "</td>");
                checklist.Append("<td>" + reader[7].ToString() + "</td>");
                checklist.Append("<td>" + reader[8].ToString() + "</td>");
                checklist.Append("<td>" + reader[9].ToString() + "</td>");
                checklist.Append("<td>" + reader[10].ToString() + "</td>");
                checklist.Append("<td>" + reader[11].ToString() + "</td>");
                checklist.Append("<td>" + reader[12].ToString() + "</td>");
                checklist.Append("<td>" + reader[13].ToString() + "</td>");
    
                checklist.Append("</tr>");
            }
            checklist.Append("</table></div>");
            BirdCheckList = checklist.ToString();
        }
        reader.Close();
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<%= BirdCheckList %>