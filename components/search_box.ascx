<%@ Control Language="C#" Debug="true" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string additional_CSS_class = "";
    public string AdditionalCSSClass {
        get { return additional_CSS_class; }
        set { additional_CSS_class = value; }
    }

    private string AdditionalBoxClass {
        get {
            if (AdditionalCSSClass.Length > 0) {
                return " searchbox_" + AdditionalCSSClass;
            }
            else {
                return "";
            }
        }
    }

    private string AdditionalButtonClass {
        get {
            if (AdditionalCSSClass.Length > 0) {
                return " searchbutton_" + AdditionalCSSClass;
            } else {
                return "";
            }
        }
    }

    private string ButtonImage {
        get {
            if (AdditionalCSSClass.Length > 0) {
                return Util.MakeUrl("web_images/search_button_small.jpg");
            } else {
                return Util.MakeUrl("web_images/search_button_large.jpg");
            }
            }
    }

    private string HoverOffset {
        get {
            if (AdditionalCSSClass.Length > 0) {
                return "-17px";
            } else {
                return "-24px";
            }
        }
    }

    private string ActiveOffset {
        get {
            if (AdditionalCSSClass.Length > 0) {
                return "-34px";
            } else {
                return "-48px";
            }
        }
    }

    private string SearchTerms {
        get {
            string result = "";
            OdbcDataReader reader = BirdWeb.SearchTerms();
            while (reader.Read()) {
                result += string.Format("{{label:\"{0}\",category:\"{1}\"}},", reader["term"].ToString(), reader["category"].ToString());
            }
            return result.TrimEnd(',');
        }
    }

</script>
<sas:db ID="BirdWeb" runat="server"></sas:db>
<form action="<%= Util.MakeUrl("searchresults") %>" method="post" id='sform'>
<fieldset>
    <legend></legend>
    <input class="searchbox<%= AdditionalBoxClass %>" type="text" size="10" name="terms"
        id="search" />
    <button class='searchbutton<%= AdditionalButtonClass %>'>
        Search</button>
</fieldset>
</form>
<script type="text/javascript">
$(function () {
         $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
				currentCategory = "";
            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    var category = "";
                    switch (item.category) {
                        case 's': category = "Birding Sites"; break;
                        case 'e': category = "Ecoregions"; break;
                        default: category = "";
                    }
                    ul.append("<li class='ui-autocomplete-category'>" + category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });
        }
    });
	        var data = [<%= SearchTerms %>];

	        $("#search").catcomplete({
	            delay: 0,
	            source: data,
                select: function(event,ui) {
                           $("#search").val(ui.item.label);
                           $(this).closest("form").submit();
                        }
	        });

            $("#sform button").css("background", "url(\"<%= ButtonImage %>\")" );
            $("#sform button").hover(
                function(){
                    $(this).css("background-position","0 <%= HoverOffset %>");
                },
                function(){
                    $(this).css("background-position","");
                }
            );
            $("#sform button").mousedown(function(){
                                  $(this).css("background-position","0 <%= ActiveOffset %>");
                                })
                              .mouseup(function(){
                                  $(this).css("background-position","");
                                  $(this).closest("form").submit(); //needed to make the button work in IE7
                              });
	    });
</script>