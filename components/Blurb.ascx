<%@ Control Language="C#" Debug="true" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string type = "";
    public string Type {
        get { return type; }
        set { type = value; }
    }

    private string image_url = "";
    private string ImageUrl {
        set {
            if (value.Equals("") ) {
                image_url = Util.MakeUrl("web_images/generic_birding_site.jpg");
            } else {
                image_url = value;
            }
        }
        get { return image_url; }
    }

    private string common_name = "";
    private string CommonName {
        set { common_name = value; }
        get { return common_name; }
    }

    private string detail_name = "";
    private string DetailName {
        set { detail_name = value; }
        get { return detail_name; }
    }

    private string blurb_text = "";
    private string BlurbText {
        set { blurb_text = value; }
        get { return blurb_text; }
    }
    
    private string LinkUrl {
        get {
            string page = "site/";
            if (Type.ToLower().Equals("bird")) {
                page = "bird/";
            }
            return Util.MakeUrl(page + Util.EncodeName(CommonName));
        }
    }
    
    private string Blurb {
        get {
            StringBuilder blurb = new StringBuilder();
            blurb.Append(string.Format("<h2><span>&nbsp;&nbsp;{0} of the Week&nbsp;&nbsp;</span></h2>", Type));
            blurb.Append(string.Format("<a href='{2}'><img alt='{0} of the week image' src='{1}'/></a>", Type, ImageUrl, LinkUrl));
            blurb.Append(string.Format("<h3><a href='{1}'>{0}</a></h3>", CommonName, LinkUrl));
            blurb.Append(string.Format("<h3 class='detail_name'><a href='{1}'>{0}</a></h3>", DetailName, LinkUrl));
            blurb.Append(string.Format("<p>{0}</p>", BlurbText));
            return blurb.ToString();
        }
    }

    void Page_Load(object Source, EventArgs e) {
        if (Type.ToLower().Equals("site")) {
            Type = "Birding Site";
        }
        Hashtable blurb_info = BirdWebDB.GetBlurbData(Type);
        ImageUrl = blurb_info["image_url"].ToString();
        BlurbText = blurb_info["blurb_text"].ToString();
        CommonName = blurb_info["common_name"].ToString();
        DetailName = blurb_info["detail_name"].ToString();
    }

</script>
<sas:db ID="BirdWebDB" runat="server"></sas:db>
<%= Blurb %>
