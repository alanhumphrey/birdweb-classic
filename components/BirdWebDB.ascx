<%@ Control Language="C#" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="BirdWeb" %>

<script runat="server">

    private string ConnectionString {
        get { return ConfigurationManager.AppSettings["connectionstring"]; }
    }

    private bool DatabaseIsMysql {
        get { return Regex.IsMatch(ConnectionString, "MySQL"); }
    }
    
    private string MakeCommandMysqlCompatible (string CommandText) {
        CommandText = Regex.Replace(CommandText, "getdate", "curdate");
        if (Regex.Match(CommandText, " top\\(1\\) ").Success) {
            CommandText = Regex.Replace(CommandText, " top\\(1\\) ", " ");
            CommandText += " limit 1";
        }
        return CommandText;
    }
    
    private OdbcDataReader conn(string CommandText) {

        OdbcConnection myConnection = null;

        try {
            if (DatabaseIsMysql) {
                CommandText = MakeCommandMysqlCompatible(CommandText); 
            }
            myConnection = new OdbcConnection(ConnectionString);
            OdbcCommand myCommand = new OdbcCommand(CommandText, myConnection);

            myConnection.Open();

            return myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        }
        catch (Exception ex) {
            myConnection.Close();
        }

        return null;
    }

    private OdbcDataReader conn(OdbcCommand CommandText) {

        try {
            if (DatabaseIsMysql) {
                CommandText.CommandText = MakeCommandMysqlCompatible(CommandText.CommandText);
            }
           
            CommandText.Connection = new OdbcConnection(ConnectionString);
            CommandText.Connection.Open();
            return CommandText.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch (Exception ex) {
            CommandText.Connection.Close();
        }
        return null;
    }

    public string getValue(object column_data) {

        string value = "No data at this time.";

        if (!column_data.Equals(DBNull.Value)) {
            value = column_data.ToString();
            string endash = Convert.ToChar(150).ToString();
            string emdash = Convert.ToChar(151).ToString();
            string leftSmartQuote = Convert.ToChar(147).ToString();
            string rightSmartQuote = Convert.ToChar(148).ToString();
            value = Regex.Replace(value, "<script.*", " ");
            value = value.Replace("\r\n", "\n").Replace("\r", "\n");
            value = Regex.Replace(value, "\n", "<br/>");
            value = Regex.Replace(value, endash, "&#8211;");
            value = Regex.Replace(value, emdash, "&#8212;");
            value = Regex.Replace(value, leftSmartQuote, "&#8220;");
            value = Regex.Replace(value, rightSmartQuote, "&#8221;");
        }
        return value;
    }

     public OdbcDataReader BirdDetail(string bird_id) {

        OdbcCommand CommandText = new OdbcCommand(@"select top(1) b.*, o.id order_id, f.id family_id, o.order_scientific_name, o.order_description,
                                  f.family_scientific_name, f.family_description, g.genus_scientific_name, sn.species_scientific_name, 
                                  b.common_name, i1.image_file na_map, i2.image_file wa_map, 
                                  sn.species_scientific_name, s.sound_file, i2.id wa_map_image_id,
                                  bb.id sts, sbi.bird_id shade, sb.bird_id survey
                              from birds b 
                                  join  genera g on b.genus_id = g.id 
                                  join families f on g.family_id = f.id 
                                  join orders o on f.order_id = o.id 
                                  join species_names sn on b.species_name_id = sn.id 
                                  left join maps m1 on m1.id = b.north_america_map_id 
                                  left join maps m2 on m2.id = b.washington_map_id 
                                  left join images i1 on i1.id = m1.image_id 
                                  left join images i2 on i2.id = m2.image_id 
                                  left join sounds s on b.default_sound_id = s.id 
                                  left join birds_breeding bb on bb.id = b.id
                                  left join shadecoffee_bird_info sbi on sbi.bird_id = b.id
                                  left join surveys_birds sb on sb.bird_id = b.id
                              where b.is_washington_bird = 1 and b.id = ?");
        CommandText.Parameters.Add("@id", OdbcType.VarChar);
        CommandText.Parameters["@id"].Value = bird_id;
        return (conn(CommandText));
    }

    public OdbcDataReader BirdIdAndPicture (string bird_name) {
        OdbcCommand command = new OdbcCommand( @"select b.id bird_id, i.image_file bird_image
                                                 from birds b
                                                 join images i on b.image_id = i.id
                                                 where url_name = ?");
        command.Parameters.AddWithValue("@name", bird_name);
        return (conn(command));
    }
    
    private string GetNameFromID(string sql, string id) {
        OdbcConnection myConn = new OdbcConnection(ConnectionString);
        OdbcCommand myCommand = new OdbcCommand(sql, myConn);
        myCommand.Parameters.Add("@id", OdbcType.VarChar);
        myCommand.Parameters["@id"].Value = id;
        myCommand.Connection.Open();
        string name = myCommand.ExecuteScalar().ToString();
        myConn.Close();
        return Util.EncodeName(name);
    }

    private string GetIDFromName(string sql, string name) {
        OdbcConnection myConn = new OdbcConnection(ConnectionString);
        OdbcCommand myCommand = new OdbcCommand( sql, myConn);
        Console.WriteLine(sql);
        myCommand.Parameters.Add("@name", OdbcType.VarChar);
        myCommand.Parameters["@name"].Value = name;
        myCommand.Connection.Open();
        object res = myCommand.ExecuteScalar();//?.ToString();
        string id = "";
        if (res != null) {
            id = res.ToString();
        }
        myConn.Close();
        return id;
    }
    
    public string BirdName(string bird_id) {
        return GetNameFromID("select url_name from birds where id = ?", bird_id);
    }

    public string BirdIDFromName(string bird_name) {
        return GetIDFromName("select id from birds where url_name = ?", bird_name);
    }

    public string SiteName( string site_id ) {
        return GetNameFromID("select url_name from birding_sites where id = ?", site_id );
    }

    public string SiteIDFromName( string site_name ) {
        return GetIDFromName("select id from birding_sites where url_name = ?", site_name);
    }

    public string EcoregionName( string site_id ) {
        return GetNameFromID("select ecoregion_name from ecoregions where id = ?", site_id);
    }

    public string EcoregionIDFromName( string ecoregion_name ) {
        return GetIDFromName("select id from ecoregions where ecoregion_name = ?", Util.DecodeName(ecoregion_name));
    }

    public string OrderName(string order_id) {
        return GetNameFromID("select order_scientific_name from orders where id = ?", order_id);
    }

    public string OrderIDFromName(string order_name) {
        return GetIDFromName("select id from orders where order_scientific_name = ?", order_name);
    }

    public string FamilyName(string family_id) {
        return GetNameFromID("select family_scientific_name from families where id = ?", family_id);
    }

    public string FamilyIDFromName(string family_name) {
        return GetIDFromName("select id from families where family_scientific_name = ?", family_name);
    }
    
    public OdbcDataReader ContributorInfo(string contributor_id) {
        OdbcCommand CommandText = new OdbcCommand("select contributor_name, public_attribution, url from contributors c " +
                             "left join urls u on c.url_id = u.id where c.id = ? ");
        CommandText.Parameters.Add("@contributor_id", OdbcType.VarChar);
        CommandText.Parameters["@contributor_id"].Value = contributor_id;
        return conn(CommandText);
    }

    // order information for the browse birds page
    public OdbcDataReader BirdOrders() {
        OdbcCommand CommandText = new OdbcCommand("select * " +
                             "from orders o " +
                             " where o.families_washington is not null" +
                             " order by taxonomic_order");

        return conn(CommandText);
    }

    // families in order the browse birds page
    public OdbcDataReader FamiliesInOrder(string order_id) {
        OdbcCommand CommandText = new OdbcCommand("select f.*, i.image_file " +
                             "from families f " +
                             " left join images i on f.silhouette_image_id = i.id " +
                             "where family_navigation_name is not null and order_id = ? " +
                             " order by taxonomic_order");
        CommandText.Parameters.Add("@order_id", OdbcType.VarChar);
        CommandText.Parameters["@order_id"].Value = order_id;
        return conn(CommandText);
    }

    public OdbcDataReader RareBirdOrders(string order_id) {

        OdbcCommand CommandText = new OdbcCommand("select f.id, order_scientific_name, family_scientific_name, family_navigation_name " +
                           "from orders o, families f " +
                           "where f.order_id = o.id and f.family_navigation_name is not null and o.id = ?" +
                           " order by f.taxonomic_order");
        CommandText.Parameters.Add("@order_id", OdbcType.VarChar);
        CommandText.Parameters["@order_id"].Value = order_id;

        return conn(CommandText);
    }

    public OdbcDataReader BirdsInFamily(string family_id) {
        OdbcCommand CommandText = new OdbcCommand(@"select b.id, b.common_name, g.genus_scientific_name, s.species_scientific_name, i2.image_file, i2.width, i2.height, i2.image_description  
                                                    from birds b 
                                                    join species_names s on b.species_name_id = s.id
                                                    join genera g on b.genus_id = g.id
                                                    join families f on g.family_id = f.id
                                                    join images i on b.image_id = i.id
                                                    join images i2 on i.master_image_id = i2.master_image_id
                                                    where f.id = ?
                                                    and i2.size_code = 'S' and b.is_washington_bird = 1
                                                    order by b.taxonomic_order");
               
        CommandText.Parameters.Add("@family_id", OdbcType.VarChar);
        CommandText.Parameters["@family_id"].Value = family_id;

        return conn(CommandText);
    }

    public OdbcDataReader RareBirdsInFamily(string family_id) {
        OdbcCommand CommandText = new OdbcCommand("select b.id, b.common_name " +
                             "from birds b, genera g, families f, " +
                             "   attributes a, bird_attributes ba " +
                             "where b.genus_id = g.id and " +
                             "g.family_id = f.id and f.id = ? " +
                             "and a.attribute_name = 'rarity' and a.id = ba.attribute_id and ba.bird_id = b.id and b.is_washington_bird = 1 " +
                             "order by b.taxonomic_order");
        CommandText.Parameters.Add("@family_id", OdbcType.VarChar);
        CommandText.Parameters["@family_id"].Value = family_id;

        return conn(CommandText);
    }

    public OdbcDataReader OrderDetail(string order_id) {
        OdbcCommand CommandText = new OdbcCommand("select * " +
                             "from orders " +
                             "where id = " + order_id);
        return conn(CommandText);
    }

    //Family List
    public OdbcDataReader FamilyDetail(string family_id) {
        OdbcCommand CommandText = new OdbcCommand("select f.*, o.order_scientific_name, i.image_file " +
                        "from families f " +
                        " join orders o on f.order_id = o.id " +
                        " left join images i on f.silhouette_image_id = i.id " +
                        "where f.id = ?");
        CommandText.Parameters.Add("@family_id", OdbcType.VarChar);
        CommandText.Parameters["@family_id"].Value = family_id;

        return conn(CommandText);
    }

    public OdbcDataReader FamilySpecies(string family_id) {
        OdbcCommand CommandText = new OdbcCommand("select b.id, b.common_name " +
                            " from birds b, genera g, families f " +
                            " where b.genus_id = g.id and " +
                            " g.family_id = f.id and f.id = ? and b.is_washington_bird = 1" +
                            " order by b.taxonomic_order");
        CommandText.Parameters.Add("@family_id", OdbcType.VarChar);
        CommandText.Parameters["@family_id"].Value = family_id;

        return conn(CommandText);
    }

    // return all birds in the same family as a given bird
    public OdbcDataReader SpeciesFamily(string bird_id) {
        OdbcCommand CommandText = new OdbcCommand(@"select b.id, b.common_name, g.genus_scientific_name, s.species_scientific_name, i2.image_file, i2.width, i2.height, i2.image_description  
                                                    from birds b 
                                                    join species_names s on b.species_name_id = s.id
                                                    join genera g on b.genus_id = g.id
                                                    join families f on g.family_id = f.id
                                                    join images i on b.image_id = i.id
                                                    join images i2 on i.master_image_id = i2.master_image_id
                                                    where f.id = (select f.id from families f, genera g, birds b 
                                                           where b.genus_id = g.id and g.family_id = f.id and b.id = ?)
                                                    and i2.size_code = 'S' and b.is_washington_bird = 1 
                                                    order by b.taxonomic_order");
        CommandText.Parameters.Add("@bird_id", OdbcType.VarChar);
        CommandText.Parameters["@bird_id"].Value = bird_id;

        return conn(CommandText);
    }

    // checklist showing ecoregions for the bird detail page.  Used by the BirdCheckList user control.
    public OdbcDataReader BirdCheckList(string bird_id) {
        OdbcCommand CommandText = new OdbcCommand("select e.id, ecoregion_name, january, february, march, april, may, june, july, " +
                             "august, september, october, november, december " +
                             "from abundancies a, birds b, ecoregions e " +
                             "where b.id = a.bird_id and e.id = a.ecoregion_id and b.id = ? and b.is_washington_bird = 1");
        CommandText.Parameters.Add("@bird_id", OdbcType.VarChar);
        CommandText.Parameters["@bird_id"].Value = bird_id;
        return conn(CommandText);
    }

    // checklist showing birds for the ecoregion detail page.  Used by the EcoregionCheckList user control.
    public OdbcDataReader EcoregionCheckList(string ecoregion_id) {
        OdbcCommand CommandText = new OdbcCommand("select b.id, b.common_name, january, february, march, april, may, june, july, " +
                             "august, september, october, november, december " +
                             "from abundancies a, birds b, ecoregions e " +
                             "where b.id = a.bird_id and e.id = a.ecoregion_id and b.is_washington_bird = 1 " +
                             "      and (january != ' ' or february != ' ' or march != ' ' or april != ' ' or may != ' ' " +
                             "           or june != ' ' or july != ' ' or august != ' ' or september != ' ' " +
                             "           or october != ' ' or november != ' ' or december != ' ' )" +
                             "      and e.id = ?" +
                             " order by b.taxonomic_order");
        CommandText.Parameters.Add("@ecoregion_id", OdbcType.VarChar);
        CommandText.Parameters["@ecoregion_id"].Value = ecoregion_id;

        return conn(CommandText);
    }

    //Image info
    public OdbcDataReader ImageInfo(string image_id) {
        OdbcCommand CommandText = new OdbcCommand("select * " +
                             "from images " +
                             "where id = " + image_id);
        return conn(CommandText);
    }


    // Rare bird orders
    public OdbcDataReader RareBirdOrders() {
        OdbcCommand CommandText = new OdbcCommand("select distinct o.id order_id, order_navigation_name " +
                             " from birds b, bird_attributes ba, attributes a, orders o, genera g, families f " +
                             " where a.attribute_name = 'rarity' and ba.attribute_id = a.id and ba.bird_id = b.id and b.genus_id = g.id and g.family_id = f.id and f.order_id = o.id order and b.is_washington_bird = 1 by order_navigation_name");
        return conn(CommandText);
    }

    // species in a given order
    public OdbcDataReader OrderSpecies(string order_id) {
        OdbcCommand CommandText = new OdbcCommand("select b.id, b.common_name " +
                             "from birds b, genera g, families f, orders o " +
                             "where b.is_washington_bird = 1 and b.genus_id = g.id and g.family_id = f.id and f.order_id = o.id and " +
                             "   o.id = ?" +
                             " order by b.taxonomic_order ");
        CommandText.Parameters.Add("@order_id", OdbcType.VarChar);
        CommandText.Parameters["@order_id"].Value = order_id;

        return conn(CommandText);
    }

    // species of special concern
    public OdbcDataReader SpeciesOfConcern() {

        // attribute codes:
        //     14 - federal
        //     18 - national audubon
        //      7 - state
        //      6 - washington_watch
        //      5 - north_america_watch
        //      4 - gap_at_risk

        OdbcCommand CommandText = new OdbcCommand("select b.id, b.common_name, a.attribute_name, ba.attribute_value " +
                             "from birds b, attributes a, bird_attributes ba " +
                             "where ba.bird_id = b.id and ba.attribute_id = a.id and a.id in (14,18,7,6,4) and b.is_washington_bird = 1 " +
                             "order by b.taxonomic_order");
        return conn(CommandText);
    }

    public OdbcDataReader SpeciesOfConcern(string bird_id) {
        OdbcCommand CommandText = new OdbcCommand(@"select b.common_name, a.attribute_name, ba.attribute_value 
                             from birds b, attributes a, bird_attributes ba 
                             where ba.bird_id = b.id and ba.attribute_id = a.id and a.id in (14,18,7,6,4) and b.is_washington_bird = 1 
                             and b.id = ?");
        CommandText.Parameters.Add("@bird_id", OdbcType.VarChar);
        CommandText.Parameters["@bird_id"].Value = bird_id;
        
        return conn(CommandText);
   }

    // summary information for the main ecoregion page
    public OdbcDataReader EcoregionSummary() {
        OdbcCommand CommandText = new OdbcCommand("select id, ecoregion_name, area " +
                             "from ecoregions " +
                             " order by id");
        return conn(CommandText);
    }

    // ecoregion detail info
    public OdbcDataReader EcoregionDetail(string ecoregion_id) {
        OdbcCommand CommandText = new OdbcCommand(@"select e.id, ecoregion_name, area, physiography, climate, habitats, human_impact, 
                                                        i1.id map, i2.id pic, i1.height height, i1.width width 
                                                    from ecoregions e 
                                                    join images i2 on e.default_ecoregion_image_id = i2.id 
                                                    left join maps m on m.id = e.map_id 
                                                    left join images i1 on m.image_id = i1.id 
                                                    where  e.id = ?");
        CommandText.Parameters.Add("@ecoregion_id", OdbcType.VarChar);
        CommandText.Parameters["@ecoregion_id"].Value = ecoregion_id;

        return conn(CommandText);
    }

    // birding sites for an ecoregion
    public OdbcDataReader SitesForEcoregion(string ecoregion_id, string order) {
        string order_by = "bs.site_name";
        if (order.Equals("map")) {
            order_by = "bse.map_number";
        }

        OdbcCommand CommandText = new OdbcCommand("select bs.id, bs.site_name, bse.map_number, e.ecoregion_name " +
                             "from birding_sites bs " +
                             "   join birding_sites_ecoregions bse on bs.id = bse.birding_site_id " +
                             "   join ecoregions e on bse.ecoregion_id = e.id " +
                             "where bse.ecoregion_id = ? " +
                             " order by " + order_by);
        CommandText.Parameters.Add("@ecoregion_id", OdbcType.VarChar);
        CommandText.Parameters["@ecoregion_id"].Value = ecoregion_id;

        return conn(CommandText);
    }

    // birding sites details
    public OdbcDataReader BirdingSiteDetail(string site_id) {
        //find an ecoregion id for this birding site.
        OdbcCommand CommandText = new OdbcCommand("select bse.ecoregion_id ecoregionid " +
                           "from birding_sites bs " +
                           " join birding_sites_ecoregions bse on bs.id = bse.birding_site_id " +
                           "where bs.id = ? ");
        CommandText.Parameters.Add("@site_id", OdbcType.VarChar);
        CommandText.Parameters["@site_id"].Value = site_id;

        string ecoregion_id = "0";
        OdbcDataReader reader = conn(CommandText);
        try {

            if (reader.HasRows) {
                reader.Read();
                ecoregion_id = reader["ecoregionid"].ToString();
            }
        }
        catch (System.Exception ex) {
            reader.Close();
        }
        finally {
            reader.Close();
        }

        return BirdingSiteDetail(site_id, ecoregion_id);
    }

    public OdbcDataReader BirdingSiteDetail(string site_id, string ecoregion_id) {
        OdbcCommand CommandText = new OdbcCommand("select bs.*, e.id ecoregionid " +
                             "from birding_sites bs " +
                             " join birding_sites_ecoregions bse on bs.id = bse.birding_site_id " +
                             " join ecoregions e on e.id = bse.ecoregion_id " +
                             "where bs.id = ? and e.id = ? " +
                             " order by bs.site_name");
        CommandText.Parameters.Add("@site_id", OdbcType.VarChar);
        CommandText.Parameters["@site_id"].Value = site_id;

        CommandText.Parameters.Add("@ecoregion_id", OdbcType.VarChar);
        CommandText.Parameters["@ecoregion_id"].Value = ecoregion_id;
        return conn(CommandText);
    }

    // birding sites resources
    public OdbcDataReader BirdingSiteSources(string site_id) {
        OdbcCommand CommandText = new OdbcCommand("select title, pages, u.url, u.last_status " +
                             "from birding_sites_sources bss, sources s left join urls u on s.url_id = u.id " +
                             "where bss.source_id = s.id and bss.birding_site_id = ? ");
        CommandText.Parameters.Add("@site_id", OdbcType.VarChar);
        CommandText.Parameters["@site_id"].Value = site_id;

        return conn(CommandText);
    }

    // Queries for search function
    private void AddParameters(ref OdbcCommand cmd, string where_clause, string[] parms) {
        int placeholder_count = where_clause.Split('?').Length - 1;
        for (int j = 0; j < placeholder_count / parms.Length; j++) {
            for (int i = 0; i < parms.Length; i++) {
                cmd.Parameters.AddWithValue("@parm", "%" + parms[i] + "%");
            }
        }
    }
    
    public OdbcDataReader SearchBirds(string where_clause, string[]parms) {
        OdbcCommand CommandText = new OdbcCommand("select b.id, b.common_name, g.genus_scientific_name, sn.species_scientific_name " +
                             "from birds b " +
                             " join species_names sn on b.species_name_id = sn.id " +
                             " join genera g on b.genus_id = g.id " +
                             " join families f on g.family_id = f.id  " +
                             " join orders o on f.order_id = o.id " +
                             "where b.is_washington_bird = 1 and ( " + where_clause + " )");
        AddParameters(ref CommandText, where_clause, parms);
        return conn(CommandText);
    }

    public OdbcDataReader SearchBirdName(string where_clause, string [] parms) {
        OdbcCommand CommandText = new OdbcCommand(@"select b.id, b.common_name, g.genus_scientific_name, 
                                                    sn.species_scientific_name, i2.image_file 
                                                    from birds b 
                                                    join genera g on b.genus_id = g.id 
                                                    join species_names sn on b.species_name_id = sn.id
                                                    join images i on b.image_id = i.id
                                                    join images i2 on i2.master_image_id = i.master_image_id
                                                    where b.is_washington_bird = 1 and i2.size_code = 'S' and " + where_clause);
        AddParameters(ref CommandText, where_clause, parms);
        return conn(CommandText);
    }

    public OdbcDataReader SearchSites(string where_clause, string[] parms) {
        OdbcCommand CommandText = new OdbcCommand("select id, site_name visible_text " +
                             "from birding_sites " +
                             "where " + where_clause +
                             " order by site_name");
        AddParameters(ref CommandText, where_clause, parms);
        return conn(CommandText);
    }

    public OdbcDataReader SearchEcoregions(string where_clause, string[] parms) {
        OdbcCommand CommandText = new OdbcCommand("select id, ecoregion_name visible_text " +
                             "from ecoregions " +
                             "where " + where_clause +
                             " order by ecoregion_name");
        AddParameters(ref CommandText, where_clause, parms);
        return conn(CommandText);
    }

    /* The search terms table is expected to have the following characteristics:
     * Bird common names come first, sorted by taxonomic order.  They do not have a category.
     * Birding sites come next, sorted by site_name.  Category code is "s".
     * Ecoregions are last, sorted by ecoregion name.  Category code is "e".
     * If any bird common name, site name, or ecoregion name changes the search_terms table must be rebuilt
     * according to this layout.  Use the stored procedure RebuildSearchTerms to rebuild the table.
     */
    public OdbcDataReader SearchTerms() {
        string CommandText = "select * from search_terms";
        return conn(CommandText);
    }

    public string SearchForTerm(string term) {
        OdbcCommand CommandText = new OdbcCommand("select * from search_terms where term = ?");
        CommandText.Parameters.AddWithValue("parm", term);
        OdbcDataReader reader = conn(CommandText);
        string result = null;
        if (reader.HasRows) {
            reader.Read();
            result = reader["category"].ToString();
        }
        reader.Close();
        return result;
    }

    // given an bird id, get the pictures - used by images control
    public OdbcDataReader BirdPictures(string bird_id) {
        OdbcCommand CommandText = new OdbcCommand(@"select i.id, i.image_file, i.width small_width, i.height small_height, i.image_description, c.contributor_name, u.url,
                                                    i2.image_file large_image_file, i2.width large_width, i2.height large_height,
                                                    i3.id xl_id, i3.width xl_width, i3.height xl_height
                                                    from bird_images bi   
                                                    join images i on bi.image_id = i.id   
                                                    join images i2 on i.master_image_id = i2.master_image_id    
                                                    join images i3 on i.master_image_id = i3.master_image_id 
                                                    join contributors c on i.contributor_id = c.id
                                                    left join urls u on c.url_id = u.id
                                                    where i.size_code = 'T' and i2.size_code = 'L' and i3.size_code='X' and bi.bird_id = ?
                                                    order by i.ranking");

        CommandText.Parameters.Add("@bird_id", OdbcType.VarChar);
        CommandText.Parameters["@bird_id"].Value = bird_id;

        return conn(CommandText);
    }

    public OdbcDataReader DefaultBirdPicture(string image_id) {
        OdbcCommand CommandText = new OdbcCommand(@"select i.*, i2.id larger_image_id, i2.image_file larger_file, i2.width larger_width, i2.height larger_height
                                                    from images i
                                                    join images i2 on i.master_image_id = i2.master_image_id
                                                    where i.id = ? and i2.size_code = 'X' ");
        CommandText.Parameters.AddWithValue("@id", image_id);
        return conn(CommandText);
    }

    public OdbcDataReader RecentlyViewedList() {
        string sDate = "";
        sDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now.AddDays(-1));

        string CommandText = "SELECT count( entry_data ) hit_count, entry_data " +
                                " FROM search_log " +
                                " WHERE entry_type =2 and entry_time ='" + sDate +
                                "' GROUP BY entry_data " +
                                " ORDER BY hit_count desc";
        return conn(CommandText);
    }

     // return sound info
    public OdbcDataReader Sound(string sound_id) {
        string CommandText = "select * " +
                             "from sounds " +
                             "where id = " + sound_id;
        return conn(CommandText);
    }

    // return an image file
    public OdbcDataReader GetPicture(string type, string id) {
        OdbcCommand CommandText = new OdbcCommand();
        switch (type) {

            case "bird":
                CommandText.CommandText = "select i.image_file from images i, birds b " +
                              "where b.image_id = i.id and b.id = ?";
                break;

            case "site":
                //fix this to use birding_sites.
                CommandText.CommandText = "select i.image_file from images i, birds b " +
                              "where b.image_id = i.id and b.id = ? ";
                break;

            case "region":
                CommandText.CommandText = "select i.image_file from images i, ecoregions e " +
                              " where e.default_ecoregion_image_id = i.id and e.id = ? ";
                break;
        }
        CommandText.Parameters.Add("@id", OdbcType.VarChar);
        CommandText.Parameters["@id"].Value = id;

        return conn(CommandText);
    }

    // site attributes
    public OdbcDataReader SiteAttributes(string site_id) {
        OdbcCommand CommandText = new OdbcCommand("select  attribute_name, attribute_value " +
                             "from site_attributes sa " +
                             " join attributes a on sa.attribute_id = a.id " +
                             "where site_id = ? ");
        CommandText.Parameters.Add("@site_id", OdbcType.VarChar);
        CommandText.Parameters["@site_id"].Value = site_id;

        return conn(CommandText);
    }

    // site habitats at risk
    public OdbcDataReader SiteHabitats(string site_id) {
        string CommandText = "select  habitat_description " +
                             "from site_habitats_at_risk shar " +
                             "  join habitat_codes hc on shar.habitat_code_id = hc.id " +
                             "where shar.birding_site_id = " + site_id;
        return conn(CommandText);
    }

    public void LogSearchTerm(string searchTerms) {
        string sDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);

        OdbcCommand cmd = new OdbcCommand("insert into search_log( entry_time, entry_type, entry_data) " +
                                           "values (?, 1, ?)");
        cmd.Parameters.Add("@entry_time", OdbcType.VarChar);
        cmd.Parameters["@entry_time"].Value = sDate;
        cmd.Parameters.Add("@entry_data", OdbcType.VarChar);
        cmd.Parameters["@entry_data"].Value = searchTerms;

        cmd.Connection = new OdbcConnection(ConnectionString);
        cmd.CommandType = CommandType.Text;
        try {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
        catch {
            if (cmd.Connection.State == ConnectionState.Open)
                cmd.Connection.Close();
        }
    }


    // audio source info
    public OdbcDataReader AudioSources() {
        string CommandText = "select b.id, b.common_name, c1.contributor_name, c2.contributor_name " +
                             "from sounds s " +
                             "  left join contributors c1 on s.recordist_id = c1.id " +
                             "  join contributors c2 on s.contributor_id = c2.id " +
                             "  join birds b on b.default_sound_id = s.id " +
                             "  where b.is_washington_bird = 1 " +
                             "order by b.common_name";

        return conn(CommandText);
    }

    public Hashtable GetBlurbData(string blurb_type) {
        Hashtable result = new Hashtable();
        if (blurb_type.ToLower().Equals("bird")) {
            result = GetBlurbForBird();
        }
        else {
            result = GetBlurbForSite();
        }
        return result;
    }

    private Hashtable GetBlurbForBird() {
        Hashtable result = new Hashtable();
        string CommandText = "select b.common_name, g.genus_scientific_name, sn.species_scientific_name, i.image_file, bl.blurb_text  " +
            "from blurbs bl " +
            "left join birds b on bl.type_id = b.id " +
            "left join genera g on g.id = b.genus_id " +
            "left join species_names sn on sn.id = b.species_name_id " +
            "left join images i on i.id = b.image_id " +
            "where bl.week = DATEPART(week, GetDate()) and bl.blurb_type = 'bird' and bl.site='birdweb' " +
            "order by bl.show_through asc";

        OdbcDataReader reader = conn(CommandText);

        if (reader.HasRows) {
            reader.Read();
            result.Add("common_name", reader["common_name"]);
            result.Add("detail_name", String.Format("{0} {1}", reader["genus_scientific_name"], reader["species_scientific_name"]));
            result.Add("image_url", String.Format("{0}", reader["image_file"]));
            result.Add("blurb_text", reader["blurb_text"]);
        } else {
            result.Add("common_name", "");
            result.Add("detail_name", "");
            result.Add("image_url", "");
            result.Add("blurb_text", "");
        }

        reader.Close();

        return result;
    }

    private Hashtable GetBlurbForSite() {
        Hashtable result = new Hashtable();

        string CommandText = @"select bs.site_name, e.ecoregion_name, i.image_file, bl.blurb_text 
                               from blurbs bl 
                               left join birding_sites bs on bl.type_id = bs.id 
                               left join birding_sites_ecoregions bse on bse.birding_site_id = bs.id 
                               left join ecoregions e on e.id = bse.ecoregion_id 
                               left join images i on i.id = bs.default_site_image_id 
                               where bl.week = DATEPART(week, GetDate()) and bl.blurb_type = 'site' and bl.site='birdweb' 
                               order by bl.show_through asc";
        
        OdbcDataReader reader = conn(CommandText);

        if (reader.HasRows) {
            reader.Read();
            result.Add("common_name", reader["site_name"]);
            result.Add("detail_name", reader["ecoregion_name"]);
            result.Add("image_url", String.Format("{0}", reader["image_file"]));
            result.Add("blurb_text", reader["blurb_text"]);
        } else {
            result.Add("common_name", "");
            result.Add("detail_name", "");
            result.Add("image_url", "");
            result.Add("blurb_text", "");
        }

        reader.Close();

        return result;
    }

</script>
