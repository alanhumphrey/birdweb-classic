<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>

<script runat="server">

    string searchTerms;
    public string SearchTerms {
        set {
            string temp = value;
            temp = Regex.Replace(temp, @"^\s*(.*?)\s*$", "$1"); //remove leading and trailing spaces
            temp = Regex.Replace(temp, @"'", "''"); //double up single quotes
            temp = Regex.Replace( temp, "update|delete|select|create|drop|xp_|--|;", "", RegexOptions.IgnoreCase );
            searchTerms = temp;
            }

        get {return searchTerms; }
    }

    private string[] IndividualSearchTerms {
        get { return Regex.Split(SearchTerms, @"[^\w-']+"); }
    }
    string searchFor;
    public string SearchFor {
        set {
                if (value.ToLower() == "birds" ||
                    value.ToLower() == "sites" ||
                    value.ToLower() == "ecoregions" ) {
                    searchFor = value.ToLower();
                } else {
                    searchFor = "birds";
                }
        }
        get { return searchFor; }
    }

    private string DetailPage() {
        string page = "";
        switch ( SearchFor ) {
            case "birds" :
                page = "bird_details.aspx?value=search&id=" ;
                break;

            case "sites" :
                page = "birding_site_details.aspx?value=search&id=" ;
                break;

            case "ecoregions" :
                page = "ecoregion_details.aspx?value=search&id=" ;
                break;
        }

        return page;
    }

    private string DetailPage(string name) {
        string page = "";
        switch (SearchFor) {
            case "birds" :
                page = Util.MakeUrl("bird/" + Util.EncodeName(name));
                break;
        }
        return page;
    }
    
    public string WhereClause() {
        ArrayList columns = GetColumns();

        StringBuilder where = new StringBuilder();
        string template = ClauseTemplate( "OR" );
        foreach (string colname in columns ) {
            where.Append("(");
            where.Append( Regex.Replace( template, "COLUMNNAME", colname ) );
            where.Append(") OR ");
        }
        where.Remove( ( where.Length - 4 ), 4 ); //remove final OR

        return where.ToString();
    }


    private string ClauseTemplate( string conjunction ) {
        string [] clause = new string [ IndividualSearchTerms.Length ];
        int i = 0;
        foreach (string currentTerm in IndividualSearchTerms) {
            clause[i++] = " COLUMNNAME LIKE ? ";
        }
        return string.Join( " " + conjunction + " ", clause ) ;
    }

    private ArrayList GetColumns() {
        ArrayList columns = new ArrayList();
        switch (SearchFor) {
            case "birds":
                columns.Add("identification");
                columns.Add("sn.species_scientific_name");
                columns.Add("g.genus_scientific_name");
                columns.Add("f.family_scientific_name");
                columns.Add("o.order_scientific_name");
                columns.Add("b.common_name");
                columns.Add("voice");
                columns.Add("habitat");
                columns.Add("behavior");
                columns.Add("diet");
                columns.Add("nesting");
                columns.Add("migration");
                columns.Add("where_found");
                columns.Add("conservation_status");
                columns.Add("breeding");
                columns.Add("notes");
                break;

            case "sites":
                columns.Add("site_name");
                columns.Add("site_description");
                columns.Add("site_birds");
                break;

            case "ecoregions":
                columns.Add("ecoregion_name");
                columns.Add("area");
                columns.Add("physiography");
                columns.Add("climate");
                columns.Add("habitats");
                columns.Add("human_impact");
                break;

        }
        return columns;
    }

    private string NameWhereClause() {
        ArrayList columns = new ArrayList();
        columns.Add("b.common_name");

        StringBuilder where = new StringBuilder();
        string template = ClauseTemplate( "AND" );
        foreach (string colname in columns ) {
            where.Append("(");
            where.Append( Regex.Replace( template, "COLUMNNAME", colname ) );
            where.Append(") AND ");
        }
        where.Remove( ( where.Length - 5 ), 5 ); //remove final AND
        return where.ToString();
    }

    private string ScienceWhereClause() {
        ArrayList columns = new ArrayList();
        columns.Add("g.genus_scientific_name");
        columns.Add("sn.species_scientific_name");

        StringBuilder where = new StringBuilder();
        string template = ClauseTemplate( "OR" );
        foreach (string colname in columns ) {
            where.Append("(");
            where.Append( Regex.Replace( template, "COLUMNNAME", colname ) );
            where.Append(") OR ");
        }
        where.Remove( ( where.Length - 4 ), 4 ); //remove final OR

        return where.ToString();
    }

    private string SearchForBirds(){
        OdbcDataReader reader = BirdWeb.SearchBirdName(NameWhereClause(), IndividualSearchTerms);

        Hashtable primeHits = new Hashtable();
        Hashtable searchHits = new Hashtable();
        string results = "";

        while ( reader.Read() ) {
            object [] values = new object [5];
            reader.GetValues(values);
            primeHits[ reader[0].ToString() ] = values ;
        }
        reader.Close();

        if ( primeHits.Count == 0 ) {
            // no luck with the common name, try scientific names
            reader = BirdWeb.SearchBirdName(ScienceWhereClause(), IndividualSearchTerms);
            while ( reader.Read() ) {
                object [] values = new object [5];
                reader.GetValues(values);
                primeHits[ reader[0].ToString() ] = values;
            }
        }
        reader.Close();

        reader = BirdWeb.SearchBirds( WhereClause(), IndividualSearchTerms );
        while ( reader.Read() ) {
            string bird_id = reader[0].ToString();
            if ( primeHits.Contains(bird_id) ) {
                // make sure we have the default common name, don't duplicate the result
                object [] values = (object [])primeHits[bird_id];
                values[1] = reader[1];
                primeHits[bird_id] = values;
            } else {
                object [] values = new object [4];
                reader.GetValues(values);
                searchHits[ bird_id ] = values ;
            }
        }
        reader.Close();

        if ( primeHits.Count > 0 ) {
            results += "<div><h2 class='searchresultHeader'>Birds</h2><ul>" ;
            IDictionaryEnumerator en = primeHits.GetEnumerator();
            while ( en.MoveNext() ) {
                object [] data = ( object [] ) en.Value;
                results += "<li class='birdphotos'>" ;
                results += string.Format( "<a href='{0}'><img src='{1}'/><span>{2}</span></a>", DetailPage(data[1].ToString()), data[4].ToString(), data[1].ToString() );
                results += string.Format( "<span class='latinname'>{0} {1}", data[2].ToString(), data[3].ToString() );
                results += "</span></li>";
            }
            results += "</ul></div>";
        }

        if ( searchHits.Count > 0 ) {
            results += "<h2 class='searchresultHeader'>Other Birds</h2><ul>" ;
            IDictionaryEnumerator en = searchHits.GetEnumerator();
            while ( en.MoveNext() ) {
                object [] data = ( object [] ) en.Value;
                results += string.Format( "<li><a href='{0}{1}'> {2}</a> - <span class='latinname'>{3} {4}</span></li>", DetailPage(), en.Key, data[1].ToString(), data[2].ToString(), data[3].ToString() ) ;
            }
            results += "</ul>";

        }

        return results;
    }

    string Search( string searchFor ) {
        SearchFor = searchFor;
        OdbcDataReader reader ;
        if ( searchFor.Equals( "sites" ) ) {
            reader = BirdWeb.SearchSites(WhereClause(), IndividualSearchTerms);
        } else {
            reader = BirdWeb.SearchEcoregions(WhereClause(), IndividualSearchTerms);
        }

        string results = "";
        if ( reader.HasRows ) {
            results += "<ul>";
            while ( reader.Read() ) {
                results += string.Format("<li><a href='{0}{1}'>{2}</a></li>", DetailPage(), reader["id"].ToString(), reader["visible_text"].ToString() );
            }
            results += "</ul>";

        }
        reader.Close();

        return results;
    }


    string SearchResultsTable() {
        string table = "";
        string temp = "";
        bool haveResults = false;
        table += "<table id='searchresult'><tr><td>";
        temp = SearchForBirds();
        if ( temp.Length > 0 ) {
            haveResults = true;
            table += temp;
            temp = "";
        }

        temp = Search( "sites" );
        if ( temp.Length > 0 ) {
            haveResults = true;
            table += "<h2 class='searchresultHeader'>Birding Sites</h2>";
            table += temp;
            temp = "";
        }

        temp = Search( "ecoregions" );
        if ( temp.Length > 0 ) {
            haveResults = true;
            table += "<h2 class='searchresultHeader'>Ecoregions</h2>";
            table += temp;
            temp = "";
        }

        if ( !haveResults ) {

            table += "<form action='searchresults' method='post'> " +
                     "<input id='searchbox' type='text' size='10' name='terms' value='" + SearchTerms + "' />" +
                     "<input id='gobtn' type='image' src='web_images/go.gif' /> " +
                     "</form><br/><br/>";
            table += "<p>Your search yielded no results.  Please enter a new keyword in the search box above.</p>";
        }
        table += "</td></tr></table>";
        return table;
    }

</script>

<sas:db id="BirdWeb" runat="server">
</sas:db>
<%= SearchResultsTable () %>

