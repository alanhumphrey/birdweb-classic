<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    private string ecoregionID;
    public string EcoregionID {
        get { return ecoregionID ; }
        set { ecoregionID = value; }
    }

    private string ecoregionchecklist;
    public string Ecoregionchecklist {
        get { return ecoregionchecklist ; }
        set { ecoregionchecklist = value; }
    }


    void Page_Load(object Source, EventArgs e) {

        OdbcDataReader reader = BirdWeb.EcoregionCheckList(EcoregionID) ;

        StringBuilder checklist = new StringBuilder();
        checklist.Append("<table>");
        checklist.Append("<caption>C=Common; F=Fairly Common; U=Uncommon; R=Rare; I=Irregular</caption>");
        checklist.Append("<tr><th>Bird</th><th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>May</th><th>Jun</th><th>Jul</th><th>Aug</th><th>Sep</th><th>Oct</th><th>Nov</th><th>Dec</th></tr>");

        bool altrow = false;
        while ( reader.Read() ) {
            checklist.Append("<tr" + (altrow ? " class=altrow>" : ">" ) );
            altrow = ! altrow;
            string url = string.Format("bird/{0}",Util.EncodeName(reader["common_name"].ToString()));
            checklist.Append( String.Format( "<th class='birdname'><a href='{0}'>{1}</a></th>", Util.MakeUrl( url), reader["common_name"].ToString() ) );
            checklist.Append("<td>" + reader[2].ToString() + "</td>");
            checklist.Append("<td>" + reader[3].ToString() + "</td>");
            checklist.Append("<td>" + reader[4].ToString() + "</td>");
            checklist.Append("<td class=altcol>" + reader[5].ToString() + "</td>");
            checklist.Append("<td class=altcol>" + reader[6].ToString() + "</td>");
            checklist.Append("<td class=altcol>" + reader[7].ToString() + "</td>");
            checklist.Append("<td>" + reader[8].ToString() + "</td>");
            checklist.Append("<td>" + reader[9].ToString() + "</td>");
            checklist.Append("<td>" + reader[10].ToString() + "</td>");
            checklist.Append("<td class=altcol>" + reader[11].ToString() + "</td>");
            checklist.Append("<td class=altcol>" + reader[12].ToString() + "</td>");
            checklist.Append("<td class=altcol>" + reader[13].ToString() + "</td>");

            checklist.Append("</tr>");
        }
        reader.Close();
        checklist.Append("</table>");
        Ecoregionchecklist = checklist.ToString();
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<div id="concern"><%= Ecoregionchecklist %>
</div>
