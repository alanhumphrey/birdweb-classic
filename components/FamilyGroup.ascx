<%@ Control Language="C#" %>
<%@ Register TagPrefix="sas" TagName="db" Src="BirdWebDB.ascx" %>
<%@ Import Namespace="BirdWeb" %>
<script runat="server">

    string orderID ;
    public string OrderID {
        set { orderID = value; }
        get { return orderID; }
    }

    void Page_Load(object Source, EventArgs e) {

        familygroup.DataSource = BirdWeb.FamiliesInOrder( OrderID );
        familygroup.DataBind();
    }

</script>
<sas:db id="BirdWeb" runat="server"></sas:db>
<div class="familylist">
    <asp:Repeater id="familygroup" runat="Server">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <a href="<%# Util.MakeUrl("family/" + Util.EncodeName( DataBinder.Eval( Container.DataItem, "family_scientific_name" ).ToString() ) )%>"><img height="42" width="42" src="<%# Util.MakeUrl(DataBinder.Eval( Container.DataItem, "image_file" ).ToString() ) %>" /></a>
                <a href="<%# Util.MakeUrl("family/" + Util.EncodeName( DataBinder.Eval( Container.DataItem, "family_scientific_name" ).ToString() ) )%>"><%# DataBinder.Eval( Container.DataItem, "family_navigation_name" ) %> (Family <%# DataBinder.Eval( Container.DataItem, "family_scientific_name" ) %>)</a>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>
