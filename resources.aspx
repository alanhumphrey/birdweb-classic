<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="Birding Resources" %>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <h2 class="qlink">
        Birding Resources
    </h2>
    <ul>
        <li><a href="#etiquette">Birding Etiquette</a> </li>
        <li><a href="#chapter">Find Your Local Chapter</a> </li>
        <li><a href="#around">Getting Around</a> </li>
        <li><a href="#ppp">Permits, Passes And Parking</a> </li>
        <li><a href="#report">Reporting Banded Birds</a> </li>
        <li><a href="#info">Other Good Info</a> </li>
        <li><a href="#festivals">Bird Festivals</a> </li>
        <li><a href="#bibliography">Bibliography</a> </li>
    </ul>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">
    <div id="resources">
        <h1>
            Birding Resources
        </h1>
        <p>
            Select from one of the following resources to learn more about various aspects of
            birds and birding in Washington State. The information provided on this web site
            is subject to change. It is not meant to be definitive or exhaustive. We welcome
            your comments and input. Please e-mail birdweb@birdsconnectsea.org.
        </p>
        <h2 class="details">
            Going birding? Be prepared.
        </h2>
        <p>
            If you're meeting a group, go a few minutes early. Wear seasonally appropriate,
            and quiet, clothing and footwear. Always take enough food and water for the length
            of the trip. Be considerate of others, and stay with the group; forging ahead may
            scare away the birds and make it impossible for others to see. Lock your car. Hide
            your valuables. Respect private property. Use common sense.
        </p>
        <h2 class="details">
            <a name="etiquette">The Etiquette and Ethics of Birding</a>
        </h2>
        <p>
            We support the American Birding Association's <a href="http://www.aba.org/about/ethics.html" target="_blank">
                Code of Birding Ethics.</a>
        </p>
        <p>Before using an audio device (like a bird audio smartphone app) in the field, we recommend reading <a href="http://www.sibleyguides.com/2011/04/the-proper-use-of-playback-in-birding/" target="_blank">&ldquo;The Proper Use of Playback in Birding&rdquo; by David Allen Sibley.</a></p>
        <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="chapter">Audubon Chapters </a>
        </h2>
        <p>
            Take a field trip with the local Audubon chapter! <a href="http://www.audubon.org/search-by-zip" target="_blank">
                Find the chapter</a> nearest your destination.
        </p>
        <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="around">Getting Around</a>
        </h2>
        <ul>
            <li>How's traffic? <a href="http://betatraffic.wsdot.wa.gov/traffic/default.aspx" target="_blank">Check
                Washington State roads.</a> </li>
            <li>What exit was that? <a href="http://wsdot.wa.gov/traffic/interstateguide" target="_blank">Interstate
                exits across Washington State.</a> </li>
            <li><a href=" https://tidesandcurrents.noaa.gov/tide_predictions.html?gid=1415" target="_blank">Tide Tables</a> for
                the Washington Coast and Puget Sound </li>
            <li><a href="http://www.wsdot.wa.gov/ferries/" target="_blank">Washington State Ferry Schedules</a> </li>
        </ul>
        <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="ppp">Permits, Passes, and Parking</a>
        </h2>
        <p>
            Passes and Permits -- What you need to know before you go.
        </p>
        <ul>
            <li><a href="http://www.discoverpass.wa.gov/" target="_blank">The Discover Pass</a> is required for Washington state parks, water-access points, heritage sites, wildlife and natural areas, trails and trailheads.</li>
            <li><a href="https://www.discovernw.org/recreation-pass-guide.html" target="_blank">Which
                recreation pass will you need at your destination?</a> </li>
            <li><a href="https://www.wta.org/go-outside/passes/what-pass-do-i-need-faq" target="_blank">Information</a> about permits for national parks and wildlife refuges, Washington public lands, US Forest Service lands, etc.
            </li>
            <li><a href="http://www.fws.gov/duckstamps/Info/Stamps/stampinfo.htm" target="_blank">Federal Duck Stamps</a> are also accepted at national wildlife refuges, and they support waterfowl conservation.
</li>
        </ul>
        
        <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="report">Reporting Banded Birds</a>
        </h2>
        <p>
            <a href='http://www.pwrc.usgs.gov/bbl/' target="_blank">USGS Patuxent Wildlife Research Center Bird Banding Laboratory</a><br />
            12100 Beech Forest Road<br />
            Laurel, MD 20708-4037<br />
            Telephone: 301-497-5790<br />
            Fax: 301-497-5717
        </p>
        <p>
             <a href='http://courses.washington.edu/vseminar/main.htm#8Reporting' target="_blank">Banded Crows in Seattle</a>
        </p>
 
         <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="info">Other Good Information</a>
        </h2>
        <ul>
            <li><a href="http://www.parks.wa.gov/" target="_blank">Washington State Parks</a></li>
            <li><a href="http://www.fws.gov/refuges/" target="_blank">National Wildlife Refuges</a> </li>
            <li><a href="http://www.wta.org/trail-news/signpost/accessible-trails " target="_blank">Washington State Trail
                Recreation Information for the Disabled </a></li>
            <li><a href="http://wnps.org/plant_lists/exploring_native_plants.html" target="_blank">Lists of Washington
                Native Plants </a></li>
        </ul>
        <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="festivals">Washington State Bird Festivals</a>
        </h2>
        <ul>
            <li><a href="http://www.skagiteagle.org" target="_blank">Upper Skagit Bald Eagle Festival, late January
                or early February </a></li>
            <li><a href="http://www.othellosandhillcranefestival.org/" target="_blank">Othello Sandhill Crane Festival,
                late March</a></li>
            <li><a href="http://www.olympicbirdfest.org" target="_blank">Olympic Peninsula BirdFest, late March/early
                April</a></li>
            <li><a href="http://www.wabrant.org/" target="_blank">Washington Brant Festival, Semiahmoo,
                mid-April</a></li>
            <li><a href="http://www.shorebirdfestival.com/" target="_blank">Grays Harbor Audubon Shorebird Festival,
                late April/early May</a></li>
            <li><a href="http://www.leavenworthspringbirdfest.com/" target="_blank">Leavenworth Spring Birdfest,
                mid-May</a></li>
            <li><a href="http://www.pugetsoundbirdfest.com" target="_blank">Puget Sound Bird Fest, Edmonds, mid-May</a></li>
            <li><a href="http://www.wenasaudubon.org/" target="_blank">Audubon Wenas Campout, Early Summer</a></li>
            <li><a href="http://www.ridgefieldfriends.org/birdfest.php" target="_blank">Ridgefield Bird Fest, mid-October</a>
            </li>
        </ul>
        <p class="top">
            <a href="#">Back to top</a>
        </p>
        <h2 class="details">
            <a name="bibliography">Bibliography</a>
        </h2>
        <ul>
            <li><i>Benchmark Washington Road &amp; Recreation Atlas</i> </li>
            <li><i>A Birders Guide to Washington</i> by Hal Opperman, ABA </li>
            <li><i>Birder's Guide to Coastal Washington</i> by Bob Morse </li>
            <li><i>The Birders Handbook A Field Guide to the Natural History of North American Birds</i>
            </li>
            <li><i>Birding in King County</i> by Eugene Hunn </li>
            <li><i>Birding in the San Juan Islands</i> (The Mountaineers, 1987) </li>
            <li><i>Birding Washington</i> by Rob and Natalie McNair-Huff </li>
            <li><i>(NEW) Birds of North America, Focus Guide </i>by Kenn Kaufman </li>
            <li><i>Birds of Washington; Status and Distribution</i> by Terence R. Wahl, William
                Tweit, and Steven G. Mlodinow </li>
            <li><i>The Birds of Yakima County, Washington</i> by Andrew Stepniewski </li>
            <li><i>Important Bird Areas of Washington</i> by Tim Cullinan </li>
            <li><i>Lives of North American Birds</i> by Kenn Kaufman </li>
            <li><i>National Geographic Field Guide to the Birds of North America</i> </li>
            <li><i>The Sibley Guide to Bird Life and Behavior</i> by David Sibley </li>
            <li><i>The Sibley Guide to Birds</i> by David Sibley </li>
            <li><i>Sibley's Birding Basics</i> by David Sibley </li>
            <li><i>Washington Atlas &amp; Gazetteer</i> by DeLorme </li>
            <li><i>The Work of Nature: How the Diversity of Life Sustains Us</i> by Paul Ehrlich
            </li>
        </ul>
        <p>
        </p>
        <p class="top">
            <a href="#">Back to top</a>
        </p>
    </div>
</asp:Content>
