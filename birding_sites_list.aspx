<%@ Page Language="C#" MasterPageFile="birdweb_interior.master" Title="BirdWeb - Ecoregion Detail"
    Debug="true" %>

<%@ Register TagPrefix="sas" TagName="checklist" Src="components/EcoregionCheckList.ascx" %>
<%@ Register TagPrefix="sas" TagName="ecoregionlist" Src="components/EcoregionList.ascx" %>
<%@ Register TagPrefix="sas" TagName="sitelist" Src="components/SiteList.ascx" %>
<%@ Register TagPrefix="sas" TagName="bwabout" Src="components/about.ascx" %>
<%@ Register TagPrefix="sas" TagName="db" Src="components/BirdWebDB.ascx" %>
<%@ Register TagPrefix="sas" TagName="image" Src="components/image.ascx" %>
<%@ Import Namespace="BirdWeb" %>

<script runat="server">
    string ecoregion_name;
    public string EcoregionName {
        set { ecoregion_name = value; }
        get { return ecoregion_name; }
    }

    void Page_Load( object Source, EventArgs e ) {
        Emphasis = Request.QueryString["emphasis"];
        if ( Request.QueryString["id"] != null ) {
            string ecoregion_id = Request.QueryString["id"];
            string ecoregion_name = BirdWebDB.EcoregionName( ecoregion_id );
            Response.RedirectPermanent( "ecoregion/sites/" + ecoregion_name + "/" + Emphasis, true );
        }
        string region_id = BirdWebDB.EcoregionIDFromName( Request.QueryString["name"] );
        Detail.DataSource = BirdWeb.EcoregionDetail( region_id );
        Detail.DataBind();
        SideDetail.DataSource = BirdWebDB.EcoregionDetail( region_id );
        SideDetail.DataBind();
        HtmlMeta description = new HtmlMeta();
        HtmlHead head = ( HtmlHead ) Page.Header;
        description.Name = "Description";
        description.Content = "Description of ecoregion.";
        head.Controls.Add( description );
        head.Title = EcoregionName;
    }


    string emphasis = "ecoregion";
    public string Emphasis {
        get { return emphasis; }
        set {
            if ( value != null ) {
                emphasis = value;
            }
        }
    }

    string leftNav( string position ) {
        string menuItem = "<li><a href='#sites'>Map and Birding Sites</a></li>";
        if ( ( position.Equals( "top" ) && !Emphasis.Equals( "ecoregion" ) ) ||
             ( position.Equals( "bottom" ) && Emphasis.Equals( "ecoregion" ) ) ) {

            return menuItem;

        } else {
            return "";
        }
    }

    string LinkText( string map_id, string width, string height, string ecoregion_id ) {
        string image_url = Util.MakeUrl( string.Format("web_images/ecoregion-birdingsite-map_{0}_off.jpg", ecoregion_id) );
        string map_image = string.Format( "<img class='ro' src='{0}' alt='picture' height='143' width=215'>", image_url );

        if ( map_id.Equals( "No data at this time." ) || width.Equals( "No data at this time." ) || height.Equals( "No data at this time." ) ) {
            return map_image;
        } else {
            string url = string.Format("{0}", Util.MakeUrl("bigger_image.aspx"));
            string window_parameters = string.Format( "'toolbar=no,scrollbars=yes, width={0}, height={1}'", Convert.ToInt32( width ) + 50, Convert.ToInt32( height ) + 75 );
            string script = string.Format( "onClick=\"MyWindow=window.open('{3}?id={0}&type={1}','MyWindow', {2}); return false;\">", map_id, "M", window_parameters,url );
            string link = string.Format( "<a href='#'{0}", script );
            return string.Format( "{1}{0}</a><p>{1}Click</a> to see a detailed map with birding sites.", map_image, link );
        }
    }


</script>

<asp:Content ID='sidenavcontent' ContentPlaceHolderID="sidenav" runat="server">
    <sas:db ID="BirdWebDB" runat="server"></sas:db>
    <asp:Repeater ID="SideDetail" runat="server">
        <ItemTemplate>
            <div class="qlink">
                <%# EcoregionName = BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "ecoregion_name" ) ) %>
                Ecoregion
            </div>
            <ul>
                <%= leftNav("top") %>
                <li><a href="#location">Location</a> </li>
                <li><a href="#physiography">Physiography</a> </li>
                <li><a href="#climate">Climate</a> </li>
                <li><a href="#habitats">Habitats</a> </li>
                <li><a href="#human_impact">Human Impact</a> </li>
                <li><a href="#checklist">Bird Checklist </a></li>
                <%= leftNav("bottom") %>
            </ul>
            <sas:ecoregionlist ID="EcoregionsList" EcoregionID='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) %>'
                LinkType="sites" ShowCurrentEcoregion="false" MapNumbers="false" runat="server" />
            <sas:bwabout ID="about" runat="server"></sas:bwabout>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID='maincontent' ContentPlaceHolderID="content" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            $("img.ro").hover(
                function () { this.src = this.src.replace("_off", "_on"); },
                function () { this.src = this.src.replace("_on", "_off"); }
            );
        });

    </script>

    <sas:db ID="BirdWeb" runat="server"></sas:db>
    <asp:Repeater ID="Detail" runat="server">
        <ItemTemplate>
            <h1>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "ecoregion_name" ) ) %>
                Ecoregion and Birding Sites
            </h1>
            <table class="mapimage">
                <tr>
                    <td>
                        <%# LinkText(BirdWeb.getValue(Eval("map")),BirdWeb.getValue(Eval("width")), BirdWeb.getValue(Eval("height")), BirdWeb.getValue(Eval("id")) ) %>
                    </td>
                    <td>
                        <sas:sitelist ID="SiteList" Ecoregion='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) %>'
                            runat="server" />
                    </td>
                </tr>
            </table>
            <h2 class="details">
                <a name="location"></a>Location
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "area" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <p>
                <sas:image ID="imageCtrl" runat="server" ImageID='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "pic" ) ) %>' ImageType='E' />
            </p>
            <h2 class="details">
                <a name="physiography"></a>Physiography
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "physiography" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <h2 class="details">
                <a name="climate"></a>Climate
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "climate" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <h2 class="details">
                <a name="habitats"></a>Habitats
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "habitats" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <h2 class="details">
                <a name="human_impact"></a>Human Impact
            </h2>
            <p>
                <%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "human_impact" ) ) %>
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            <h2 class="details" id="checklist">
                <a name="checklist"></a><a href='<%= Util.MakeUrl("abundancecode/ecoregion")%>' target='_blank'>
                    <img id='info' src='<%= Util.MakeUrl("web_images/information.png")%>' title='Abundance Code Definitions' alt='Abundance Code Definitions' /></a>Bird
                Checklist
            </h2>
            <p>
                <sas:checklist ID="CheckList" runat="server" EcoregionID='<%# BirdWeb.getValue(DataBinder.Eval(Container.DataItem, "id" ) ) %>' />
            </p>
            <p class="top">
                <a href="#">back to top</a>
            </p>
            </p>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
