﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;

namespace BirdWeb {
    /// <summary>
    /// Any common utilities for BirdWeb go here.
    /// </summary>
    public class Util {
        public Util() {
            //
            // TODO: Add constructor logic here
            //
        }
        /// <summary>
        /// Use MakeUrl whenever you need a full url to a resource.  URL rewriting can
        /// wreak havoc on relative paths.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns> full path to the resource e.g. http://birdweb.org/birdweb/images/bird_image.jpg </returns>
        public static string MakeUrl(string filename) {
            HttpRequest req = HttpContext.Current.Request;
            string filepath = Regex.Replace(req.FilePath, "(.*)/.*$", "$1");
            string url = req.Url.GetLeftPart(UriPartial.Authority) + filepath;

            return string.Format("{0}/{1}", url, filename);
        }

        /// <summary>
        /// Make the name suitable for putting in a url
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string EncodeName(string name) {
            name = Regex.Replace( name, "/", "_" );
            name = Regex.Replace(name, "'", "");
            return HttpUtility.UrlEncode(Regex.Replace(name, " ", "_").ToLower());
        }

        /// <summary>
        /// Make the input suitable for display or for search
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string DecodeName(string name) {
            return HttpUtility.UrlDecode( Regex.Replace(name, "_", " ") );
        }
        
        public static HtmlMeta NewMetaTag(string name, string content) {
            HtmlMeta meta = new HtmlMeta();
            meta.Attributes.Add("property", name);
            meta.Attributes.Add("content", content);
            return meta;
        }

        public static void WriteMetaTag(System.Web.UI.HtmlControls.HtmlHead pHead, string pName, string pContent) {
            //Remove the MetaTag if it is already set
            //Loop through the controls in the head
            for (int i = pHead.Controls.Count - 1; i >= 0; i--) {
                //If the current head control is an HtmlMeta object, inspect it
                if (pHead.Controls[i] is HtmlMeta) {
                    //Cast the current control to an HtmlMeta
                    HtmlMeta thisMetaTag = (HtmlMeta)pHead.Controls[i];

                    //If the name matches the name passed into the method remove it
                    if (thisMetaTag.Name == pName) {
                        pHead.Controls.RemoveAt(i);
                    }
                }
            }

            //Declare a new meta control
            HtmlMeta metaTag = new HtmlMeta();

            //Set the name of the meta tag
            metaTag.Name = pName;

            //Set the content of the meta tag
            metaTag.Content = pContent;

            //Add the meta tag to the control collection
            pHead.Controls.Add(metaTag);
        }
    }
}